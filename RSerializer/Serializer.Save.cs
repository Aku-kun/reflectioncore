﻿using RSerializer.Common;

using System;
using System.Collections;
using System.Reflection;
using System.Text;

namespace RSerializer
{
    public partial class Serializer : IDisposable
    {
        private int Id { get; set; }
        private StringBuilder Builder { get; set; }

        private void BeforeSerialize(object obj)
        {
            Id = 0;
            Builder = new StringBuilder();
            SetId(obj);
        }

        public string Serialize(object obj)
        {
            BeforeSerialize(obj);
            SerializeObj(obj);

            return Builder.ToString();
        }

        private void SetId(object obj, bool isReference = false)
        {
            Type type = obj?.GetType();

            if (type?.GetCustomAttribute<RSerializableAttribute>() != null)
            {
                if (obj is IReference reference)
                {
                    if (reference.Id == 0)
                        reference.Id = Id++;
                    else if (isReference)
                        return;
                }

                foreach (PropertyInfo property in type.GetProperties())
                {
                    if (property.SetMethod == null || property.GetCustomAttribute<IgnoreAttribute>() != null)
                        continue;

                    object value = property.GetValue(obj);
                    isReference = property.GetCustomAttribute<ReferenceAttribute>() != null;

                    if (value is IEnumerable enumerable)
                    {
                        foreach (object item in enumerable)
                            SetId(item, isReference);

                        continue;
                    }

                    SetId(value, isReference);
                }
            }
        }

        private void SerializeObj(object obj)
        {
            Type type = obj?.GetType();

            if (type?.GetCustomAttribute<RSerializableAttribute>() != null)
            {
                Builder.Append($"({obj.GetType()})");

                foreach (PropertyInfo property in type.GetProperties())
                {
                    if (property.SetMethod == null || property.GetCustomAttribute<IgnoreAttribute>() != null)
                        continue;

                    object value = property.GetValue(obj);

                    Builder.Append($"<[{property.Name}]=");
                    SerializeValue(value, property);
                    Builder.Append(">");
                }
            }
            else if (type.IsPrimitive || type.IsEnum || (type.IsValueType && type.IsSerializable))
                Builder.Append($"({type}){obj}");
        }

        private void SerializeValue(object value, PropertyInfo valueProperty)
        {
            if (value == null)
                return;

            if (value is string str)
                Builder.Append($"(string[{str.Length}]){str}");
            else if (value is IEnumerable enumerable)
            {
                Builder.Append($"({value.GetType().FullName})");

                int index = 0;
                if (value is IDictionary dictionary)
                {
                    IEnumerator keys = dictionary.Keys.GetEnumerator();
                    IEnumerator values = dictionary.Values.GetEnumerator();

                    for (index = 0; index < dictionary.Count; index++)
                    {
                        keys.MoveNext();
                        values.MoveNext();

                        Builder.Append($"<[{index}]=");
                        SerializeValue(keys.Current, valueProperty);
                        Builder.Append("|");
                        SerializeValue(values.Current, valueProperty);
                        Builder.Append(">");
                    }
                }
                else
                    foreach (object item in enumerable)
                    {
                        Builder.Append($"<[{index++}]=");
                        SerializeValue(item, valueProperty);
                        Builder.Append(">");
                    }
            }
            else if (valueProperty?.GetCustomAttribute<ReferenceAttribute>() != null && value is IReference reference)
                Builder.Append($"({reference.GetType()})<[{nameof(reference.Id)}]=({reference.Id.GetType()}){reference.Id}>");
            else
                SerializeObj(value);
        }

        public void Dispose()
            => Builder?.Clear();
    }
}
