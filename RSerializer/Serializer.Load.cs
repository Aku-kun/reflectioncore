﻿using RSerializer.Common;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RSerializer
{
    public partial class Serializer
    {
        private List<IReference> Sources { get; set; }

        private void AfterDeserialize(object obj)
        {
            Sources = new List<IReference>();

            GetSource(obj);
            SetReference(obj);
        }

        public T Deserialize<T>(string data)
        {
            T result = (T)DeserializeObject(Activator.CreateInstance(typeof(T)), data);
            AfterDeserialize(result);

            return result;
        }

        private void GetSource(object obj, bool isReference = false)
        {
            Type type = obj?.GetType();

            if (type?.GetCustomAttribute<RSerializableAttribute>() != null)
            {
                if (!isReference && obj is IReference reference)
                    Sources.Add(reference);

                foreach (PropertyInfo property in type.GetProperties())
                {
                    if (property.SetMethod == null || property.GetCustomAttribute<IgnoreAttribute>() != null)
                        continue;

                    object value = property.GetValue(obj);
                    isReference = property.GetCustomAttribute<ReferenceAttribute>() != null;

                    if (value is IEnumerable enumerable)
                    {
                        foreach (object item in enumerable)
                            GetSource(item, isReference);

                        continue;
                    }

                    GetSource(value, isReference);
                }
            }
        }

        private void SetReference(object obj)
        {
            Type type = obj?.GetType();

            if (type?.GetCustomAttribute<RSerializableAttribute>() != null)
            {
                foreach (PropertyInfo property in type.GetProperties())
                {
                    if (property.SetMethod == null || property.GetCustomAttribute<IgnoreAttribute>() != null)
                        continue;

                    object value = property.GetValue(obj);
                    bool isReference = property.GetCustomAttribute<ReferenceAttribute>() != null;

                    if (value is string)
                        continue;

                    if (value is IEnumerable enumerable)
                    {
                        if (value is IDictionary dictionary)
                        {
                            IDictionary rDictionary = (IDictionary)Activator.CreateInstance(value.GetType());

                            IEnumerator keys = dictionary.Keys.GetEnumerator();
                            IEnumerator values = dictionary.Values.GetEnumerator();

                            for (int i = 0; i < dictionary.Count; i++)
                            {
                                keys.MoveNext();
                                values.MoveNext();

                                object item = values.Current;

                                if (isReference && item is IReference referenceItem)
                                {
                                    object sourceItem = Sources.FirstOrDefault(s => s.Id == referenceItem.Id);

                                    if (sourceItem != null)
                                        rDictionary.Add(keys.Current, sourceItem);

                                    continue;
                                }

                                SetReference(item);
                                rDictionary.Add(keys.Current, item);
                            }

                            property.SetValue(obj, rDictionary);
                        }
                        else
                        {
                            Type itemType = enumerable.GetType().GenericTypeArguments[0];
                            List<object> items = new List<object>();

                            foreach (object item in enumerable)
                            {
                                if (isReference && item is IReference referenceItem)
                                {
                                    object sourceItem = Sources.FirstOrDefault(s => s.Id == referenceItem.Id);

                                    if (sourceItem != null)
                                        items.Add(sourceItem);

                                    continue;
                                }

                                SetReference(item);
                                items.Add(item);
                            }

                            Type enumerableType = typeof(Enumerable);
                            MethodInfo castMethod = enumerableType.GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(itemType);
                            MethodInfo toListMethod = enumerableType.GetMethod(nameof(Enumerable.ToList)).MakeGenericMethod(itemType);

                            object castedItems = castMethod.Invoke(null, new[] { items });
                            property.SetValue(obj, toListMethod.Invoke(null, new[] { castedItems }));
                        }

                        continue;
                    }

                    if (isReference && value is IReference referenceValue)
                    {
                        object sourceValue = Sources.FirstOrDefault(s => s.Id == referenceValue.Id && s.GetType() == referenceValue.GetType());

                        if (sourceValue != null)
                            property.SetValue(obj, sourceValue);

                        continue;
                    }

                    SetReference(value);
                }
            }
        }

        private object DeserializeObject(object obj, string data)
        {
            Type type = obj?.GetType();

            if (type?.GetCustomAttribute<RSerializableAttribute>() != null)
            {
                foreach (KeyValuePair<string, string> param in Parse(data))
                {
                    PropertyInfo property = type.GetProperty(param.Key);

                    if (property != null)
                        property.SetValue(obj, DeserializeValue(param.Value));
                }

                return obj;
            }

            return Convert.ChangeType(data, type);
        }

        private object DeserializeValue(string data)
        {
            if (string.IsNullOrEmpty(data))
                return null;

            Type type = null;

            if (data.StartsWith("(string["))
            {
                data = data.Remove(0, "(string[".Length);

                string lenString = data.Substring(0, data.IndexOf(']'));
                int len = int.Parse(lenString);

                data = data.Substring(2 + lenString.Length, len);
                type = typeof(string);
            }
            else
            {
                data = data.Remove(0, 1);
                type = SerializerHelper.GetType(data.Substring(0, data.IndexOf(')')));
                data = data.Remove(0, data.IndexOf(')') + 1);
            }

            if (type == null)
                throw new ArgumentNullException();

            if (string.IsNullOrEmpty(data))
                return Convert.ChangeType(null, type);

            if (type == typeof(string))
                return data;

            object value = Activator.CreateInstance(type);

            if (value is IEnumerable enumerable)
            {
                List<object> items = new List<object>();

                if (value is IDictionary dictionary)
                {
                    Type keyType = dictionary.GetType().GenericTypeArguments[0];
                    Type valueType = dictionary.GetType().GenericTypeArguments[1];

                    foreach (KeyValuePair<string, string> param in Parse(data, true))
                    {
                        string item = param.Value;
                        string k, v;

                        if (item.StartsWith("(string["))
                        {
                            string lenString = item.Substring("(string[".Length, item.IndexOf(']') - "(string[".Length);
                            int len = int.Parse(lenString);

                            k = item.Substring(0, len + $"(string[{len}])".Length);
                            v = item.Substring(k.Length + 1);
                        }
                        else
                        {
                            k = item.Substring(0, item.IndexOf('|'));
                            v = item.Substring(k.Length + 1);
                        }

                        dictionary.Add(DeserializeValue(k), DeserializeValue(v));
                    }

                    return dictionary;
                }

                Type itemType = enumerable.GetType().GenericTypeArguments[0];

                foreach (KeyValuePair<string, string> param in Parse(data))
                    items.Add(DeserializeValue(param.Value));

                Type enumerableType = typeof(Enumerable);
                MethodInfo castMethod = enumerableType.GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(itemType);
                MethodInfo toListMethod = enumerableType.GetMethod(nameof(Enumerable.ToList)).MakeGenericMethod(itemType);

                object castedItems = castMethod.Invoke(null, new[] { items });
                return toListMethod.Invoke(null, new[] { castedItems });
            }

            if (type.IsEnum)
                return Enum.Parse(value.GetType(), data);

            if (value is Guid)
                return new Guid(data);

            return DeserializeObject(value, data);
        }

        private Dictionary<string, string> Parse(string data, bool isDictionary = false)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(data))
                return result;

            if (data[0] == '(')
                data = data.Remove(0, data.IndexOf(')') + 1);
            
            while (data.Length > 0)
            {
                string key, value;
                
                data = data.Remove(0, 2);
                key = data.Substring(0, data.IndexOf(']'));
                data = data.Remove(0, key.Length + 2);

                int start = 1;
                int end = 0;
                int index = 0;

                for (int i = 0; i < data.Length; i++)
                {
                    if (data.Substring(i).StartsWith("(string["))
                    {
                        i += "(string[".Length;
                        string str = data.Substring(i);
                        string lenString = str.Substring(0, str.IndexOf(']'));
                        i += 2 + lenString.Length + int.Parse(lenString);
                    }

                    if (data[i] == '<')
                        start++;
                    else if (data[i] == '>')
                        end++;

                    if (start == end)
                    {
                        index = i;
                        break;
                    }
                }

                value = data.Substring(0, index);
                data = data.Remove(0, value.Length);

                result.Add(key, value);

                if (!string.IsNullOrEmpty(data) && data[0] == '>')
                    data = data.Remove(0, 1);
            }

            return result;
        }
    }
}
