﻿using System;

namespace RSerializer.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class RSerializableAttribute : Attribute { }
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreAttribute : Attribute { }
    [AttributeUsage(AttributeTargets.Property)]
    public class ReferenceAttribute : Attribute { }
}
