﻿using Log;

using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace RSerializer.Common
{
    public static class SerializerHelper
    {
        public static void Save(string data, string path = "__data")
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);

                using (FileStream stream = new FileStream(path, FileMode.CreateNew))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(data);
                    stream.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Save data", ex);
            }
        }

        public static string Load(string path = "__data")
        {
            try
            {
                if (File.Exists(path))
                    return File.ReadAllText(path);
                
                return default;
            }
            catch (Exception ex)
            {
                Logger.Log("Load data", ex);
            }

            return default;
        }

        internal static Type GetType(string typeName)
        {
            Type type = Type.GetType(typeName);

            if (type != null)
                return type;

            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }

            throw new ArgumentNullException();
        }
    }
}
