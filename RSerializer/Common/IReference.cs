﻿namespace RSerializer.Common
{
    public interface IReference
    {
        int Id { get; set; }
    }
}
