﻿using System;
using System.IO;

namespace Log
{
    public static class Logger
    {
        public static event Action<object, Exception> OnLog;

        private const string LogFileName = "__log";

        static Logger()
        {
            try
            {
                if (File.Exists(LogFileName))
                {
                    long size = new FileInfo(LogFileName).Length;
                    if (size > 1048576)
                    {
                        File.Delete(LogFileName);
                        Log($"Clear log [old size: {size}]");
                    }
                }
            }
            catch { }
        }

        public static void Log(object log, Exception ex = null, bool ignoreEvent = false)
        {
            try
            {
                if (!ignoreEvent)
                    OnLog?.Invoke(log, ex);

                using (StreamWriter stream = new StreamWriter(LogFileName, true))
                {
                    stream.WriteLine($"[{DateTime.Now.ToString("dd.MM hh:mm:ss")}] {(ex != null ? "[Error] " : "")}{log}{(ex != null ? $" | {ex.Message}{Environment.NewLine}{ex.StackTrace}" : "")}");
                }
            }
            catch { }
        }
    }
}
