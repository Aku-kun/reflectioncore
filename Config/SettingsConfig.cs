﻿using LangSupport;

using RSerializer.Common;

using System;

namespace Config
{
    [RSerializable]
    public class SettingsConfig
    {
        public const string PROJECTS_PATH = ".p";
#if DEBUG
        public const string CLIENT_PATH = @"../../../Reflection.Client/bin/Debug/Reflection.Client.exe";
#else
        public const string CLIENT_PATH = "Client/client.exe";
#endif
        public const string EXE = ".exe";
        public const string CONFIG = ".rd";
        public const string BIN = "bin";
        public const string RESOURCES = "Resources";

        public SettingsConfig() 
            => ProjectName = "New Project";

        public string ProjectName { get; set; }
        public string ProjectPath { get; set; }

        public DateTime LastUpdate { get; set; }
    }
}
