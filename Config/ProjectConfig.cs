﻿using LangSupport;

using Log;

using ReflectionCore.Interface;
using ReflectionCore.Visual;
using ReflectionCore.Visual.Interface;

using RSerializer;
using RSerializer.Common;

using System;
using System.Collections.Generic;

namespace Config
{
    [RSerializable]
    public class ProjectConfig
    {
        public ProjectConfig()
        {
            Funcs = new List<IBlock>();
            Pages = new List<IPage>();
        }

        public static ProjectConfig Load(string data)
        {
            try
            {
                ProjectConfig config = null;

                using (Serializer serializer = new Serializer())
                {
                    config = serializer.Deserialize<ProjectConfig>(data);

                    config.Funcs = config.Funcs ?? new List<IBlock>();
                    config.Pages = config.Pages ?? new List<IPage>();
                    config.Lang = config.Lang ?? new LangConfig();
                    config.Visual = config.Visual ?? new VisualConfig
                    {
                        IsDark = true,
                        PrimaryColor = -16757440,
                        SecondaryColor = -16757440
                    };

                    return config;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Load project", ex);
            }

            return default;
        }

        public string Save()
        {
            try
            {
                using (Serializer serializer = new Serializer())
                {
                    return serializer.Serialize(this);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Save lang", ex);
            }

            return default;
        }


        public VisualConfig Visual { get; set; }
        public LangConfig Lang { get; set; }

        public List<IBlock> Funcs { get; set; }
        public List<IPage> Pages { get; set; }

        public object Meta { get; set; }
    }

    public class ResourcesMeta
    {
    }
}
