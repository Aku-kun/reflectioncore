﻿using ReflectionCore.Entity.Core;
using ReflectionCore.Entity.Core.Data;
using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.MSSQL
{
    public partial class Model : ISQLModel, IModel
    {
        public SQLObjects Type { get; set; }
        public string Name { get; set; }
        public ICollection<Field> Fields { get; set; }

        public object GetValue(string name)
        {
            throw new NotImplementedException();
        }

        public string SetValue(string name, object value)
        {
            throw new NotImplementedException();
        }
    }
}
