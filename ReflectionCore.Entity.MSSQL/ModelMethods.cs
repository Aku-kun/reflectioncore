﻿using ReflectionCore.Entity.Core;
using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.MSSQL
{
    public partial class Model
    {
        public ISQLModel Alter(string objectName, object parameter)
        {
            throw new NotImplementedException();
        }

        public void BuildQuery()
        {
            throw new NotImplementedException();
        }

        public ISQLModel Count()
        {
            throw new NotImplementedException();
        }

        public ISQLModel Create(string objectName, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Delete()
        {
            throw new NotImplementedException();
        }

        public ISQLModel Drop(string objectName)
        {
            throw new NotImplementedException();
        }

        public object Execute()
        {
            throw new NotImplementedException();
        }

        public ISQLModel GroupBy(string field)
        {
            throw new NotImplementedException();
        }

        public bool Has()
        {
            throw new NotImplementedException();
        }

        public bool Has(string table)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Insert(string[] fileds, object[] vaules)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Join(JoinTypes type, string rightTable, string leftField, string condition, string rightField)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Join(string leftTable, JoinTypes type, string rightTable, string leftField, string condition, string rightField)
        {
            throw new NotImplementedException();
        }

        public ISQLModel OrderBy(string filed)
        {
            throw new NotImplementedException();
        }

        public ISQLModel OrderBy(string filed, SortOperators oper)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Select(params string[] columns)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Uodate(string[] fileds, object[] vaules)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Where(string field, object value)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Where(string field, object value, string condition)
        {
            throw new NotImplementedException();
        }

        public ISQLModel Where(string field, object value, string condition, CondOperators oper)
        {
            throw new NotImplementedException();
        }
    }
}
