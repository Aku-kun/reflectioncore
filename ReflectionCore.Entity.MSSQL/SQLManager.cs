﻿using ReflectionCore.Entity.Core.Data;
using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.MSSQL
{
    public class SQLManager : ISQLManager
    {
        private SqlConnection sqlConnection;

        public void SetConnection(DBConfig config)
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    sqlConnection.Dispose();   
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
