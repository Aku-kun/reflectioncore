﻿using MaterialDesignThemes.Wpf;

using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Reflection.IDE.Interface
{
    public interface IMenu
    {
        bool IsReadOnly { get; set; }

        event Action<IMenuItem> OnSelect; 
        event Action<IMenuItem> OnRemove;
        Func<string, bool> OnAdd { get; }

        string Name { get; }
        string Hint { get; }
        PackIconKind Icon { get; }
        ObservableCollection<IMenuItem> Items { get; }

        void AddItem(IMenuItem item);
        void RemoveItem(IMenuItem item);
    }

    public interface IMenuItem
    {
        bool HideEdit { get; set; }
        bool HideRemove { get; set; }

        Func<IMenuItem, bool> OnEdit { get; }

        Action Click { get; set; }
        ICommand Remove { get; set; }

        string Name { get; set; }
        string NewName { get; set; }
        bool Select { get; set; }

        PackIconKind Icon { get; }

        object Context { get; set; }
        object Content { get; set; }
    }
}
