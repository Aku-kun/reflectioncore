﻿using ReflectionCore.Visual.Interface;

using System;
using System.Collections.Generic;

namespace Reflection.IDE.Interface
{
    public interface IVisualDesign : IVisualCode
    {
        Action Refresh { get; set; }

        IPage Page { get; set; }
        IEnumerable<IVisualProperty> Properties { get; }
    }
}
