﻿using ReflectionCore.Interface;

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Reflection.IDE.Interface
{
    public interface IVisualPin : IVisual
    {
        Action OnLoad { get; set; }

        event Action<IVisualPin> PinClick;
        void OnPinClick();

        IEnumerable<ILine> Lines { get; }

        IVisualNode Node { get; }
        IPin Pin { get; }
        object Value { get; set; }

        UIElement PrevControl { get; }
        UIElement NextControl { get; }

        void AddLine(ILine line);
        void RemoveLine(ILine line); 
        
        SolidColorBrush Color { get; }
    }
}
