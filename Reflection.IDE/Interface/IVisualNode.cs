﻿using Reflection.IDE.VisualCode;

using ReflectionCore.Interface;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Reflection.IDE.Interface
{
    public interface IVisualNode : IVisual
    {
        event Action UpdatePosition;
        event Action<IVisualNode, double, double> SelectUpdatePosition;
        event Action<IVisualNode> EndSelectUpdatePosition;

        void OnSelectUpdate();
        void OnUpdatePosition(double x, double y);
        void OnEndUpdatePosition();

        void UpdateData(INode node, out List<ILine> removeLines);

        event Action<IVisualNode, IVisualPin> InClick;
        event Action<IVisualNode, IVisualPin> OutClick;

        double X { get; set; }
        double Y { get; set; }

        DragBehavior Drag { get; set; }

        INode Node { get; }

        IVisualPin ThreadIn { get; }
        IVisualPin ThreadOut { get; }

        ObservableCollection<IVisualPin> In { get; }
        ObservableCollection<IVisualPin> Out { get; }

        bool Select { get; set; }
    }
}
