﻿using MaterialDesignThemes.Wpf;

using ReflectionCore.Interface;

using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;

using Reflection.IDE.VisualCode;
using System.Windows;

namespace Reflection.IDE.Interface
{
    public interface IVisualCode : IVisual
    {
        FrameworkElement Viewer { get; set; }

        ObservableCollection<ICodeAction> Actions { get; }
        ObservableCollection<IVisualNode> Nodes { get; }
        ObservableCollection<ILine> Lines { get; }

        void AddNode(INode node, double x = 0, double y = 0);

        void LeftClick(IVisualNode node);
        void RightClick();

        Action MoveOn { get; set; }
        Action MoveOff { get; set; }
        void Move(MouseEventArgs e);

        void KeyDown(KeyEventArgs e);

        void Init();
    }
}
