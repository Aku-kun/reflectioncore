﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Reflection.IDE.Interface
{
    public interface ILine : IVisual
    {
        Action OnLoad { get; set; }

        IVisualPin StartPin { get; }
        IVisualPin EndPin { get; }

        void SetStart(IVisualPin pin);
        void SetEnd(IVisualPin pin);
        void Cancel();

        Point Start { get; set; }
        Point End { get; set; }

        void UpdateAll();
        void UpdateEnd(Point cursor);
        void Remove();

        SolidColorBrush Color { get; }
    }
}
