﻿using ReflectionCore.Interface;

using System.Windows;

namespace Reflection.IDE.Interface
{
    public interface IVisualProperty
    {
        string Name { get; }
        IObject Source { get; }

        int Index { get; set; }
        string Group { get; set; }

        bool TopHeader { get; set; }
        FrameworkElement Control { get; set; }
        object Value { get; set; }
    }
}
