﻿using System.ComponentModel;
using System.Windows;

namespace Reflection.IDE.Interface
{
    public interface IVisual : INotifyPropertyChanged
    {
        FrameworkElement Control { get; set; }
        FrameworkElement Parent { get; set; }

        void Notify(string name = "");
    }
}
