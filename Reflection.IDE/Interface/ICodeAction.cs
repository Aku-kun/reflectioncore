﻿using MaterialDesignThemes.Wpf;

using System.Windows.Input;

namespace Reflection.IDE.Interface
{
    public interface ICodeAction
    {
        string Name { get; }
        
        double Size { get; }
        double FontSize { get; }

        PackIconKind Icon { get; }
        ICommand Command { get; }
    }
}
