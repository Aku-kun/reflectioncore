﻿using Reflection.IDE.Interface;

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode.Properties
{
    public partial class InputPropertyVisual : UserControl
    {
        public InputPropertyVisual(Func<string, bool> canChange = null)
        {
            InitializeComponent();

            if (canChange != null)
            {
                TB.PreviewTextInput += (s, e) =>
                {
                    if (string.IsNullOrEmpty(e.Text) || (e.Text == "-" && TB.CaretIndex == 0))
                        e.Handled = false;
                    else
                        e.Handled = canChange(TB.Text.Insert(TB.CaretIndex, e.Text));
                };
                DataObject.AddPastingHandler(TB, (s, e) =>
                {
                    if (e.DataObject.GetDataPresent(typeof(string)))
                    {
                        string text = (string)e.DataObject.GetData(typeof(string));
                        if (canChange(TB.Text.Insert(TB.CaretIndex, text)))
                            e.CancelCommand();
                    }
                    else
                        e.CancelCommand();
                });
            }

            TB.KeyDown += TBKeyDown;
            DataContextChanged += OnDataContextChanged;
        }

        private void TBKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                DependencyObject ancestor = TB.Parent;
                while (ancestor != null)
                {
                    if (ancestor is UIElement element && element.Focusable)
                    {
                        element.Focus();
                        break;
                    }

                    ancestor = VisualTreeHelper.GetParent(ancestor);
                }
                Keyboard.ClearFocus();
            }
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is IVisualProperty visualProperty)
                TB.Text = (visualProperty.Value ?? "").ToString();
        }
    }
}
