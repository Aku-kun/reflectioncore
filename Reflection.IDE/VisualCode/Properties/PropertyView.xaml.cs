﻿using Reflection.IDE.Interface;

using System.Windows;
using System.Windows.Controls;

namespace Reflection.IDE.VisualCode.Properties
{
    public partial class PropertyView : UserControl
    {
        public PropertyView()
        {
            InitializeComponent();
            DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is IVisualProperty visualProperty)
            {
                Value.Content = visualProperty.Control;
                if (visualProperty.TopHeader)
                {
                    Label.Visibility = Visibility.Collapsed;
                    Grid.SetRow(Value, 1);
                    Grid.SetColumn(Value, 0);
                    Grid.SetColumnSpan(Value, 3);
                }
                else
                {
                    Label.Visibility = Visibility.Visible;
                    Grid.SetRow(Value, 0);
                    Grid.SetColumn(Value, 2);
                    Grid.SetColumnSpan(Value, 1);
                }
            }
        }
    }
}
