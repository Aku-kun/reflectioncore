﻿using Reflection.IDE.Interface;

using System.Windows;
using System.Windows.Controls;

namespace Reflection.IDE.VisualCode.Properties
{
    public partial class BoolPropertyVisual : UserControl
    {
        public BoolPropertyVisual()
        {
            InitializeComponent();

            DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is IVisualProperty visualProperty)
                CB.IsChecked = (bool)visualProperty.Value;
        }
    }
}
