﻿using LangSupport;

using Reflection.IDE.Common;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Nodes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Reflection.IDE.VisualCode
{
    public partial class SelectNode : UserControl
    {
        public Action<INode> OnSelect { get; set; }

        public SelectNode()
        {
            InitializeComponent();
            MaterialDesignThemes.Wpf.HintAssist.SetHint(Search, "search".Translate());

            DataContext = new SelectNodeController(n => OnSelect?.Invoke(n));
        }

        public void Visual(bool show)
        {
            if (DataContext is SelectNodeController controller)
            {
                controller.IsVisual = show;
                controller.Notify(nameof(SelectNodeController.FilterNodes));
            }
        }
    }

    public class SelectNodeController : Base
    {
        public bool IsVisual { get; set; }

        public SelectNodeController(Action<INode> action)
        {
            Nodes = NodeHelper.AllNodes.Select(n => new NodeItemController(n, action)).ToList();
            VisualNodes = NodeHelper.VisualNodes.Select(n => new NodeItemController(n, action)).ToList();

            MainWindow.OnCustomFuncAdd += (name) =>
            {
                if (!Nodes.Any(n => n.Name == name))
                {
                    Nodes.Add(new NodeItemController(MainController.GetCustomFuncView(name), n => action?.Invoke(MainController.GetCustomFuncView(name))));
                    Notify(nameof(FilterNodes));
                }
            };

            MainWindow.OnCustomFuncRemove += (name) =>
            {
                NodeItemController node = Nodes.FirstOrDefault(n => n.Name == name);
                if (node != null)
                {
                    Nodes.Remove(node);
                    Notify(nameof(FilterNodes));
                }
            };

            MainWindow.OnCustomFuncUpdate += (name) =>
            {
                NodeItemController node = Nodes.FirstOrDefault(n => n.Name == name);
                if (node != null)
                {
                    Nodes.Remove(node);
                    Nodes.Add(new NodeItemController(MainController.GetCustomFuncView(name), n => action?.Invoke(MainController.GetCustomFuncView(name))));
                    Notify(nameof(FilterNodes));
                }
            };

            MainWindow.OnCustomFuncNameChange += (oldName, newName) =>
            {
                NodeItemController node = Nodes.FirstOrDefault(n => n.Name == oldName);
                if (node != null)
                {
                    (node.Node as Node).Name = newName;
                    node.Notify(nameof(NodeItemController.Name));
                }
            };
        }

        private List<NodeItemController> Nodes { get; }
        private List<NodeItemController> VisualNodes { get; }

        public IEnumerable<NodeItemController> FilterNodes =>
            (IsVisual ? VisualNodes.Concat(Nodes) : Nodes).Where(n => string.IsNullOrEmpty(Filter) ? true : n.Name.ToLower().Contains(Filter));

        private string filter;
        public string Filter
        {
            get => filter;
            set
            {
                if (!string.Equals(value.ToLower(), filter, StringComparison.OrdinalIgnoreCase))
                {
                    filter = value.ToLower();
                    Notify(nameof(FilterNodes));
                }
            }
        }
    }
}
