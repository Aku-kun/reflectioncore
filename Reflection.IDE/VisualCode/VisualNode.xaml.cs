﻿using Microsoft.Xaml.Behaviors;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;

using ReflectionCore.Interface;

using RSerializer.Common;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode
{
    public partial class VisualNode : BaseUserControl
    {
        public VisualNode() 
            => InitializeComponent();
    }

    public class NodeController : BaseController, IVisualNode
    {
        public event Action UpdatePosition;
        public event Action<IVisualNode> EndSelectUpdatePosition;
        public event Action<IVisualNode, double, double> SelectUpdatePosition;

        public event Action<IVisualNode, IVisualPin> InClick;
        public event Action<IVisualNode, IVisualPin> OutClick;

        public NodeController(INode node)
        {
            Node = node;

            Width = Helper.GetNodeWidth(node);
            BG = Helper.NodeColorToBrush(node.Color, 1);

            if (node.Out.Type != PinType.IObject)
            {
                PinController pin = new PinController(this, node.Out);
                pin.PinClick += OutPinClick;
                Out.Add(pin);

                if (node.In != null)
                    foreach (IPin param in node.In)
                    {
                        pin = new PinController(this, param);
                        pin.PinClick += InPinClick;
                        In.Add(pin);
                    }
            }
            else if(node.In != null)
                foreach (IPin param in node.In)
                {
                    PinController pin = new PinController(this, param, true);
                    pin.PinClick += OutPinClick;
                    Out.Add(pin);
                }

            ThreadIn = new PinController(this, node.ThreadIn);
            ThreadIn.PinClick += InPinClick;

            ThreadOut = new PinController(this, node.ThreadOut);
            ThreadOut.PinClick += OutPinClick;

            Notify(nameof(BG));
            Notify(nameof(Width));
            Notify(nameof(Name));

            Notify(nameof(ThreadIn));
            Notify(nameof(ThreadOut));
        }

        public void UpdateData(INode node, out List<ILine> removeLines)
        {
            removeLines = new List<ILine>();
            bool changeIn = false;

            if (node.In.Count() != In.Count)
                changeIn = true;
            else
            {
                foreach (IVisualPin pin in In)
                {
                    if (!node.In.Any(p => p == pin.Pin))
                    {
                        changeIn = true;
                        break;
                    }
                }
            }

            if (changeIn)
            {
                foreach (IVisualPin pin in In)
                    removeLines.AddRange(pin.Lines);

                In.Clear();
                foreach (IPin param in node.In)
                {
                    PinController pin = new PinController(this, param);
                    pin.PinClick += InPinClick;
                    In.Add(pin);
                }
            }

            if (node.Out.IsVoid)
            {
                removeLines.AddRange(Out.FirstOrDefault().Lines);
                Out.Clear();
            }
            else
            {
                if (Out.Count > 0)
                {
                    removeLines.AddRange(Out.FirstOrDefault().Lines);
                    Out.Clear();
                }

                PinController pin = new PinController(this, node.Out);
                pin.PinClick += OutPinClick;
                Out.Add(pin);
            }
        }

        private void InPinClick(IVisualPin p) => InClick?.Invoke(this, p);
        private void OutPinClick(IVisualPin p) => OutClick?.Invoke(this, p);

        public double X { get; set; }
        public double Y { get; set; }
        [Ignore]
        public DragBehavior Drag { get; set; }

        public INode Node { get; }

        public IVisualPin ThreadIn { get; }
        public IVisualPin ThreadOut { get; }

        public ObservableCollection<IVisualPin> In { get; } = new ObservableCollection<IVisualPin>();
        public ObservableCollection<IVisualPin> Out { get; } = new ObservableCollection<IVisualPin>();

        private bool select;
        public bool Select 
        {
            get => select; 
            set
            {
                select = value;

                Border = select ? Brushes.White : Brushes.Transparent;
                Notify(nameof(Border));
            }
        }

        public double Width { get; }
        public string Name => Node.Name;
        public Brush BG { get; }
        public Brush Border { get; private set; }

        public void OnUpdatePosition(double x, double y)
        {
            UpdatePosition?.Invoke();
            
            X = x;
            Y = y;

            if (Select)
                SelectUpdatePosition?.Invoke(this, x, y);
        }

        public void OnSelectUpdate() 
            => UpdatePosition?.Invoke();

        public void OnEndUpdatePosition()
        {
            if (Select)
                EndSelectUpdatePosition?.Invoke(this);
        }
    }

    public class DragBehavior : Behavior<UIElement>
    {
        private FrameworkElement Parent;
        private IVisualNode Node;
        private Point ElementStartPosition;
        private Point MouseStartPosition;

        public readonly TranslateTransform Transform = new TranslateTransform();

        protected override void OnAttached()
        {
            AssociatedObject.RenderTransform = Transform;

            if (AssociatedObject is FrameworkElement frameworkElement)
                if (frameworkElement.DataContext is IVisualNode n)
                {
                    Node = n;
                    Node.Drag = this;

                    Parent = Node.Parent;

                    Update(Node.X, Node.Y);
                    End();
                }
                else
                    frameworkElement.DataContextChanged += (s, e) =>
                    {
                        Node = frameworkElement.DataContext as NodeController;
                        Node.Drag = this;

                        Parent = Node.Parent;

                        Update(Node.X, Node.Y);
                        End();
                    };

            AssociatedObject.MouseLeftButtonDown += (s, e) =>
            {
                MouseStartPosition = e.GetPosition(Parent);
                AssociatedObject.CaptureMouse();
            };

            AssociatedObject.MouseLeftButtonUp += (sender, e) =>
            {
                AssociatedObject.ReleaseMouseCapture();

                End();
                Node.OnEndUpdatePosition();
            };

            AssociatedObject.MouseMove += (sender, e) =>
            {
                Vector diff = e.GetPosition(Parent) - MouseStartPosition;
                if (AssociatedObject.IsMouseCaptured)
                {
                    Update(diff.X, diff.Y);
                    Node.OnUpdatePosition(diff.X, diff.Y);
                }
            };
        }

        public void Update(double x, double y)
        {
            Transform.X = ElementStartPosition.X + x;
            Transform.Y = ElementStartPosition.Y + y;
        }

        public void End()
        {
            ElementStartPosition.X = Transform.X;
            ElementStartPosition.Y = Transform.Y;
        }
    }
}
