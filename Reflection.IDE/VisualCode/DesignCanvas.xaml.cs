﻿using LangSupport;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;
using Reflection.IDE.VisualCommon;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Visual.Interface;
using ReflectionCore.Visual.Wpf;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using MenuItem = Reflection.IDE.VisualCommon.MenuItem;

namespace Reflection.IDE.VisualCode
{
    public partial class DesignCanvas : UserControl
    {
        public DesignCanvas()
        {
            InitializeComponent();
            DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is IVisualDesign design)
            {
                Point position = new Point(Page.Width / 6, 100);
                MatrixTransform transform = Page.RenderTransform as MatrixTransform;
                Matrix matrix = transform.Matrix;
                double scale = 0.5;

                matrix.ScaleAtPrepend(scale, scale, position.X, position.Y);
                transform.Matrix = matrix;

                design.Refresh = () =>
                {
                    Result.Content = design.Page.Control;
                };
            }
        }

        private void CanvasMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            Point position = e.GetPosition(Page);
            MatrixTransform transform = Page.RenderTransform as MatrixTransform;
            Matrix matrix = transform.Matrix;
            double scale = e.Delta >= 0 ? 1.1 : (1.0 / 1.1);

            matrix.ScaleAtPrepend(scale, scale, position.X, position.Y);
            transform.Matrix = matrix;
        }
    }

    public class DesignController : CodeController, IVisualDesign
    {
        public Action Refresh { get; set; }

        public DesignController(List<INode> viewNodes) : base(viewNodes)
        {
            Actions.Add(new CodeAction("add_control".Translate(), PackIconKind.Add, () =>
            {
                MainWindow.ShowSelectControl(OnControlSelect);
            })); 
        }

        public override void Init()
        {
            base.Init();

            Page = new VisualPage
            {
                Name = "Page",
                IsDev = true
            };

            Page.Columns.Add(new VisualColumn());
            Page.Rows.Add(new VisualRow());

            AddControl(Page, false);
            Select(Page);
            Elements[0].Select = true;

            Refresh?.Invoke();
        }

        public void Load(IPage page, IBlock block)
        {
            Elements[0].Remove.Execute(null);
            
            Page = WpfVisualHelper.GetView(page);
            Page.IsDev = true;

            List<IControlElement> elements = Page.Elements.ToList();
            Page.Elements = new List<IControlElement>();

            AddControl(Page, false);
            Select(Page);
            Elements[0].Select = true;

            foreach (IControlElement element in elements)
                AddControl(element);

            Load(block);
            Refresh?.Invoke();
        }

        protected override bool IsVisual => true;
        protected override void OnSelect(INode node)
        {
            if (node.Type == NodeType.InputParam)
            {
                OutputController.Error("input_node_error".Translate());
                MainWindow.CloseAllDialog();
            }
            else
                base.OnSelect(node);
        }

        public IPage Page { get; set; }
        public IEnumerable<IVisualProperty> Properties { get; private set; }
        public ObservableCollection<IMenuItem> Elements { get; } = new ObservableCollection<IMenuItem>();

        public void AddControl(IControl control, bool canRemove = true)
        {
            int index = 1;
            string controlName = control.Name;
            while (Elements.Any(e => e.Name.ToLower() == controlName.ToLower()))
            {
                controlName = $"{control.Name}#{index}";
                index++;
            }
            control.Name = controlName;

            IControlElement controlElement = control as IControlElement;
            if (controlElement != null)
            {
                Page.Elements.Add(controlElement);
                Page.UpdateElements();
            }

            INode node = new Node(
                control.Name,
                NodeType.Control,
                NodeColor.Orange,
                new ThreadPin("null"),
                new ThreadPin("null"),
                new OutPin(PinType.IObject, null, true),
                control.Pins.Cast<IPin>().ToList()
            );

            AddNode(node);
            IVisualNode vn = Nodes.Last();

            IMenuItem menuItem = new MenuItem(controlElement == null ? PackIconKind.BallotOutline : PackIconKind.BorderOutside, control.Name, null)
            {
                HideEdit = true,
                HideRemove = !canRemove
            };

            menuItem.Click = () =>
            {
                foreach (IMenuItem item in Elements.Where(i => i.Select))
                    item.Select = false;

                menuItem.Select = true;
                Select(control);
            };

            menuItem.Remove = new SimpleCommand(() =>
            {
                base.RemoveNode(vn);

                Elements.Remove(menuItem); 
                if (controlElement != null)
                {
                    Page.Elements.Remove(controlElement);
                    Page.UpdateElements();
                }

                if (Elements.Count > 0 && menuItem.Select)
                {
                    Elements[0].Select = true;
                    Select(Page);
                }
            });

            Elements.Add(menuItem);

            control.OnChange += (name) =>
            {
                if (name == nameof(IControl.Name))
                {
                    if (menuItem.Name == control.Name)
                        return;

                    if (Elements.Any(e => e.Name.ToLower() == control.Name.ToLower()))
                    {
                        OutputController.Error("name_error".Translate());
                        control.Name = menuItem.Name;
                        return;
                    }

                    (vn.Node as Node).Name = control.Name;
                    vn.Notify(nameof(VisualNode.Name));

                    menuItem.Name = control.Name;
                }
            };
        }

        protected virtual void OnControlSelect(IControl control)
        {
            MainWindow.CloseAllDialog();
            control.IsDev = true;
            AddControl(control);
        }

        public void Select(IControl control)
        {
            Properties = control.Properties.Select(p => Helper.GetPropertyControl(control, p)).OrderBy(p => p.Index).ToList();
            Notify(nameof(Properties));
        }

        protected override bool CanIObjectEndLine(IVisualPin pin)
        {
            IControl control = SelectLine.StartPin.Node.Node.Name == Page.Name
                ? Page
                : (IControl)Page.Elements.FirstOrDefault(e => e.Name == SelectLine.StartPin.Node.Node.Name);

            return control == null
                ? false 
                : control.GetType().GetMethod(pin.Node.Node.Name) != null;
        }

        protected override void RemoveNode(IVisualNode visualNode, bool ignore = false)
        {
            if (!ignore && (visualNode.Node.ThreadIn.Name == "null" && visualNode.Node.ThreadOut.Name == "null"))
                return;

            base.RemoveNode(visualNode);
        }
    }
}
