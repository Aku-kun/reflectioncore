﻿using LangSupport;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;

using ReflectionCore.Interface;
using ReflectionCore.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode
{
    public partial class VisualPin : BaseUserControl
    {
        public VisualPin()
        {
            InitializeComponent();
            Loaded += (s, e) =>
            {
                if (DataContext != null && DataContext is IVisualPin pin)
                    pin.OnLoad?.Invoke();
            };
        }

        private void Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is IVisualPin pin)
                pin.OnPinClick();
        }

        protected override void OnDataContextChange()
        {
            if (DataContext is IVisualPin pin)
            {
                pin.Control = Pin;

                Prev.Content = pin.PrevControl;
                if (Prev.Content != null)
                    Prev.Visibility = Visibility.Visible;

                Next.Content = pin.NextControl;
                if (Next.Content != null)
                    Next.Visibility = Visibility.Visible;
            }
        }
    }

    public class PinController : BaseController, IVisualPin
    {
        public Action OnLoad { get; set; }

        public event Action<IVisualPin> PinClick;
        public void OnPinClick() => PinClick?.Invoke(this);

        public PinController(IVisualNode node, IPin pin, bool isVisual = false)
        {
            Node = node;
            Pin = pin;

            Color = Helper.PinTypeToBrush(Pin.Type);
            BG = Color;

            if ((pin is IOutPin outPin && outPin.IsVoid) || (pin is IThreadPin threadPin && !threadPin.IsVisible))
                Visibility = Visibility.Collapsed;
            else if (pin.Type == PinType.Thread)
            {
                Icon = PackIconKind.LogicGateAnd;
                BG = Brushes.Transparent;
            }

            if (pin is IStaticPin staticPin && Node.Node.Type != NodeType.InputParam)
                Value = staticPin.Value;

            PrevControl = Helper.GetPrevControl(isVisual ? new OutPin(pin.Type, pin.Name) : pin, Node.Node.Type == NodeType.InputParam);
            NextControl = Helper.GetNextControl(isVisual ? new OutPin(pin.Type, pin.Name) : pin);

            Notify(nameof(Visibility));
            Notify(nameof(Icon));
            Notify(nameof(Color));
            Notify(nameof(BG));
            Notify(nameof(Name));
            Notify(nameof(Value));
        }

        public string Name
            => !string.IsNullOrEmpty(Pin.Name)
                ? char.ToUpper(Pin.Name[0]) + Pin.Name.Substring(1)
                : "";

        public string Hint
            => Node.Node.Type == NodeType.InputParam
                ? "name".Translate()
                : "value".Translate();   

        public IVisualNode Node { get; }
        public IPin Pin { get; }

        private List<ILine> lines = new List<ILine>();
        public IEnumerable<ILine> Lines { get => lines; set => lines = value.ToList(); }

        private object value;
        public object Value 
        { 
            get => value; set
            {
                this.value = value;
                Notify();
            }
        }

        public UIElement PrevControl { get; }
        public UIElement NextControl { get; }

        public Visibility Visibility { get; } = Visibility.Visible;
        public PackIconKind Icon { get; } = PackIconKind.CheckboxBlankCircleOutline;
        public SolidColorBrush Color { get; }
        public SolidColorBrush BG { get; } 
        public SolidColorBrush SelectBG { get; private set; } = Brushes.Transparent;

        public void AddLine(ILine line)
        {
            lines.Add(line);
            SelectBG = line.Color;
            Notify(nameof(SelectBG));
        }

        public void RemoveLine(ILine line)
        {
            lines.Remove(line);
            if (lines.Count == 0)
            {
                SelectBG = Brushes.Transparent;
                Notify(nameof(SelectBG));
            }
        }
    }
}
