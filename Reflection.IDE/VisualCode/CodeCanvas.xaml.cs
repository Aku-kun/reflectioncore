﻿using LangSupport;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;
using ReflectionCore;
using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Nodes;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Reflection.IDE.VisualCode
{
    public partial class CodeCanvas : UserControl
    {
        public CodeCanvas() 
            => InitializeComponent();
    }

    public class CodeController : BaseController, IVisualCode
    {
        public FrameworkElement Viewer { get; set; }

        protected CodeController()
        {
            Actions.Add(new CodeAction("compile".Translate(), PackIconKind.Build, () =>
            {
                ProjectHelper.Compile();
            }, null, 20));
            Actions.Add(new CodeAction("add_node".Translate(), PackIconKind.Add, () =>
            {
                MainWindow.ShowSelectNode(OnSelect, IsVisual);
            }));
        }

        private readonly Action OnUpdate;
        private readonly Action OnInit;
        protected readonly List<INode> ViewNodes;
        public CodeController(List<INode> viewNodes) : this()
            => ViewNodes = viewNodes;

        public CodeController(List<INode> viewNodes, Action onUpdate, Action onInit) : this(viewNodes)
        {
            OnUpdate = onUpdate;
            OnInit = onInit;
        }

        public virtual void Init()
        {
            if (ViewNodes == null)
                return;

            for (int i = 0; i < ViewNodes.Count; i++)
                AddNode(ViewNodes[i], i * 50 + 50, i * 25 + 50);

            ViewNodes.Clear();
            OnInit?.Invoke();
        }

        public void Load(IBlock block)
        {
            Nodes = new ObservableCollection<IVisualNode>(Nodes.OrderBy(n => n.Node.Name));

            List<IBlockNode> items = block.Items
                .Where(i => i.Node.Type == NodeType.Control || i.Node.Type == NodeType.Start || i.Node.Type == NodeType.End)
                .OrderBy(i => i.Node.Name)
                .Concat(block.Items
                    .Where(i => i.Node.Type != NodeType.Control && i.Node.Type != NodeType.Start && i.Node.Type != NodeType.End))
                .ToList();

            int index = 0;
            foreach (IBlockNode node in items)
                if (node is SaveBlockNode saveNode)
                {
                    saveNode.Id = index;
                    if (node.Node.Type != NodeType.Control && node.Node.Type != NodeType.Start && node.Node.Type != NodeType.End)
                        AddNode(saveNode.Node, saveNode.X, saveNode.Y);
                    else
                    {
                        Nodes[index].X = saveNode.X;
                        Nodes[index].Y = saveNode.Y;
                    }

                    if (node.Node.Type == NodeType.StaticParam)
                    {
                        Nodes[index].Out[0].Value = saveNode.Value;
                        Nodes[index].Out[0].Notify(nameof(IVisualPin.Value));
                    }

                    index++;
                }

            if (Nodes.Count > 0)
            {
                IVisualNode lastNode = Nodes.Last();
                IVisualPin pin = lastNode.ThreadIn ?? lastNode.In?.FirstOrDefault();

                if (pin != null)
                    pin.OnLoad = () =>
                    {
                        pin.OnLoad = null;

                        foreach (IBlockNode node in items)
                            if (node is SaveBlockNode saveNode)
                            {
                                if (saveNode.NextNode is SaveBlockNode nextSaveNode)
                                {
                                    OutClick(Nodes[saveNode.Id], Nodes[saveNode.Id].ThreadOut);
                                    InClick(Nodes[nextSaveNode.Id], Nodes[nextSaveNode.Id].ThreadIn);
                                }

                                if (saveNode.In != null)
                                {
                                    foreach (IBlockNodePin inPin in saveNode.In)
                                        if (inPin.BlockNode != null && inPin.BlockNode is SaveBlockNode fromNode)
                                        {
                                            OutClick(Nodes[fromNode.Id], Nodes[fromNode.Id].Out.FirstOrDefault(p => p.Pin.Name == inPin.FromPin.Name));
                                            if (inPin.FromPin.Type == PinType.Thread && inPin.ToPin.Type == PinType.Thread)
                                                InClick(Nodes[saveNode.Id], Nodes[saveNode.Id].ThreadIn);
                                            else
                                                InClick(Nodes[saveNode.Id], Nodes[saveNode.Id].In.FirstOrDefault(p => p.Pin.Name == inPin.ToPin.Name));
                                        }
                                }
                            }

                        if (Lines?.Count > 0)
                        {
                            ILine lastLine = Lines.Last();
                            lastLine.OnLoad = () =>
                            {
                                lastLine.OnLoad = null;

                                foreach (ILine line in Lines)
                                    line.UpdateAll();
                            };
                        }
                            
                    };
            }
        }

        public virtual void AddNode(INode node, double x = 0, double y = 0)
        {
            IVisualNode visualNode = new NodeController(node)
            {
                X = x,
                Y = y,
                Parent = Viewer
            };

            visualNode.InClick += InClick;
            visualNode.OutClick += OutClick;
            visualNode.SelectUpdatePosition += SelectUpdatePosition;
            visualNode.EndSelectUpdatePosition += EndSelectUpdatePosition;

            if (node is ICustomNode)
            {
                void remove(string name)
                {
                    if (name == node.Name)
                    {
                        RemoveNode(visualNode);
                        MainWindow.OnCustomFuncRemove -= remove;
                    }
                }
                MainWindow.OnCustomFuncRemove += remove;

                void nameChange(string oldName, string newName)
                {
                    if (oldName == node.Name)
                    {
                        (visualNode.Node as CustomNode).Name = newName;
                        visualNode.Notify(nameof(CustomNode.Name));
                        MainWindow.OnCustomFuncNameChange -= nameChange;
                    }
                }
                MainWindow.OnCustomFuncNameChange += nameChange;

                void update(string name)
                {
                    if (name == node.Name)
                    {
                        visualNode.UpdateData(MainController.GetCustomFuncView(name), out List<ILine> removeLines);
                        RemoveLines(removeLines);
                    }
                }
                MainWindow.OnCustomFuncUpdate += update;
            }

            Nodes.Add(visualNode);

            if (node.Type == NodeType.InputParam)
            {
                OnUpdate?.Invoke();
                visualNode.Out.FirstOrDefault().PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == nameof(PinController.Value))
                        OnUpdate?.Invoke();
                };
            }
        }

        protected virtual bool IsVisual { get; } = false;
        protected virtual void OnSelect(INode node)
        {
            MainWindow.CloseAllDialog();
            AddNode(node);
        }

        private void SelectUpdatePosition(IVisualNode visualNode, double x, double y)
        {
            foreach (IVisualNode visual in Nodes.Where(n => n.Select && n != visualNode))
            {
                visual.OnSelectUpdate();
                visual.Drag.Update(x, y);
            }
        }

        private void EndSelectUpdatePosition(IVisualNode visualNode)
        {
            foreach (IVisualNode visual in Nodes.Where(n => n.Select && n != visualNode))
                visual.Drag.End();
        }

        protected ILine SelectLine { get; set; }
        protected void InClick(IVisualNode node, IVisualPin pin)
        {
            if (SelectLine != null)
            {
                if (SelectLine.StartPin.Node == node)
                    return;

                if (pin.Pin.Type != PinType.Object && pin.Pin.Type != SelectLine.StartPin.Pin.Type)
                    return;

                if (pin.Pin.Type != PinType.Thread && pin.Lines.Any())
                    return;

                if (pin.Pin.Type == PinType.IObject && !CanIObjectEndLine(pin))
                    return;

                MoveOff?.Invoke();

                SelectLine.SetEnd(pin);
                SelectLine = null;

                if (NodeHelper.IsReturn(pin.Node.Node))
                    OnUpdate?.Invoke();
            }
            else
                RemoveLines(pin.Lines.ToList());
        }

        protected virtual bool CanIObjectEndLine(IVisualPin pin)
            => false;

        protected void OutClick(IVisualNode node, IVisualPin pin)
        {
            if (SelectLine == null)
            {
                if (pin.Pin.Type == PinType.Thread && pin.Lines.Any())
                    return;

                SelectLine = new LineController(Viewer);
                SelectLine.SetStart(pin);
                Lines.Add(SelectLine);

                MoveOn?.Invoke();
            }
        }

        public void Move(MouseEventArgs e)
            => SelectLine?.UpdateEnd(e.GetPosition(Viewer));

        public ObservableCollection<ICodeAction> Actions { get; } = new ObservableCollection<ICodeAction>();
        public ObservableCollection<IVisualNode> Nodes { get; private set; } = new ObservableCollection<IVisualNode>();
        public ObservableCollection<ILine> Lines { get; private set; } = new ObservableCollection<ILine>();

        public Action MoveOn { get; set; }
        public Action MoveOff { get; set; }

        public virtual void KeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                foreach (IVisualNode visualNode in Nodes.Where(n => n.Select).ToList())
                    RemoveNode(visualNode);
        }

        public virtual void LeftClick(IVisualNode node)
        {
            if (node == null || !Keyboard.IsKeyDown(Key.LeftShift))
                foreach (IVisualNode visualNode in Nodes)
                    visualNode.Select = false;

            if (node != null)
                node.Select = !node.Select;
        }

        public virtual void RightClick()
        {
            if (SelectLine != null)
            {
                SelectLine.Cancel();
                Lines.Remove(SelectLine);
                SelectLine = null;
            }
        }

        protected virtual void RemoveNode(IVisualNode visualNode, bool ignore = false)
        {
            if (!ignore && (visualNode.Node.Type == NodeType.Start || visualNode.Node.Type == NodeType.End))
                return;

            visualNode.InClick -= InClick;
            visualNode.OutClick -= OutClick;
            visualNode.SelectUpdatePosition -= SelectUpdatePosition;
            visualNode.EndSelectUpdatePosition -= EndSelectUpdatePosition;

            RemoveLines(visualNode.ThreadIn.Lines.ToList());
            RemoveLines(visualNode.ThreadOut.Lines.ToList());

            foreach (IVisualPin visualPin in visualNode.In)
                RemoveLines(visualPin.Lines.ToList());

            foreach (IVisualPin visualPin in visualNode.Out)
                RemoveLines(visualPin.Lines.ToList());

            Nodes.Remove(visualNode);

            if (visualNode.Node.Type == NodeType.InputParam)
                OnUpdate?.Invoke();
        }

        protected virtual void RemoveLines(List<ILine> lines)
        {
            foreach (ILine line in lines)
            {
                line.Remove();
                Lines.Remove(line);

                if (NodeHelper.IsReturn(line.EndPin.Node.Node) && line.EndPin.Lines.Count() == 0)
                    OnUpdate?.Invoke();

            }
        }
    }
}
