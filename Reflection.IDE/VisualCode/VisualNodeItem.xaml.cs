﻿using Reflection.IDE.Common;
using Reflection.IDE.VisualCommon;

using ReflectionCore.Interface;

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode
{
    public partial class VisualNodeItem : UserControl
    {
        private readonly Window parent = Application.Current.MainWindow;

        public VisualNodeItem()
        {
            InitializeComponent();
            VisualClick.LeftClick(this, _ =>
            {
                if (DataContext is NodeItemController node)
                    node.Select?.Invoke(node.Node);
            });
        }
    }

    public class NodeItemController : Base
    {
        public Action<INode> Select { get; }

        public INode Node { get; }

        public Brush BG { get; }
        public SolidColorBrush TextColor { get; } = Brushes.White;

        public NodeItemController(INode node, Action<INode> select)
        {
            Node = node;
            Select = select;
            BG = Helper.NodeColorToBrush(node.Color, 1);
        }

        public string Name => Node.Name;
        public string Group => Node.Type.ToString();
    }
}
