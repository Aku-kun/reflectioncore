﻿using Reflection.IDE.Common;
using Reflection.IDE.Interface;

using System;
using System.Windows;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode
{
    public partial class Line : BaseUserControl
    {
        public Line()
        {
            InitializeComponent();
            Loaded += (s, e) =>
            {
                if (DataContext != null && DataContext is ILine line)
                    line.OnLoad?.Invoke();
            };
        }
    }

    public class LineController : BaseController, ILine
    {
        public Action OnLoad { get; set; }

        private Point PinCenter => new Point(8, 8);

        public LineController(FrameworkElement parent) 
            => Parent = parent;

        public void SetStart(IVisualPin pin)
        {
            StartPin = pin;
            Color = Helper.PinTypeToBrush(StartPin.Pin.Type);
            StartPin.AddLine(this);

            Start = StartPin.Control.TranslatePoint(PinCenter, Parent);
            End = Start;

            Update();
        }

        public void SetEnd(IVisualPin pin)
        {
            EndPin = pin;
            EndPin.AddLine(this);

            StartPin.Node.UpdatePosition += UpdateStart;
            EndPin.Node.UpdatePosition += UpdateEnd;

            End = EndPin.Control.TranslatePoint(PinCenter, Parent); 

            Update();
        }

        public void Cancel()
            => StartPin.RemoveLine(this);

        public IVisualPin StartPin { get; private set; }
        public IVisualPin EndPin { get; private set; }

        private SolidColorBrush color = Brushes.White;
        public SolidColorBrush Color
        {
            get => color;
            set
            {
                color = value;

                Notify();
            }
        }

        public Point Start { get; set; }
        public Point Start1 { get; set; }
        public Point End { get; set; }
        public Point End1 { get; set; }

        public void Remove()
        {
            StartPin.RemoveLine(this);
            EndPin.RemoveLine(this);

            StartPin.Node.UpdatePosition -= UpdateStart;
            EndPin.Node.UpdatePosition -= UpdateEnd;
        }

        public void UpdateAll()
        {
            Start = StartPin.Control.TranslatePoint(PinCenter, Parent);
            End = EndPin.Control.TranslatePoint(PinCenter, Parent);

            Update();
        }

        private void UpdateStart()
        {
            Start = StartPin.Control.TranslatePoint(PinCenter, Parent);
            Update();
        }

        private void UpdateEnd()
        {
            End = EndPin.Control.TranslatePoint(PinCenter, Parent);
            Update();
        }

        public void UpdateEnd(Point point)
        {
            End = new Point(point.X + PinCenter.X, point.Y + PinCenter.Y);
            Update();
        }

        private void Update()
        {
            Vector line = End - Start;

            double partX = line.X * (1.0 / 4.0);
            double partY = line.Y * (1.0 / 4.0);

            if (Start.X > End.X)
            {
                Start1 = new Point(Start.X + partX, Start.Y + 3 * partY + (partY * 0.5));
                End1 = new Point(Start.X + 3 * partX, Start.Y + partY * 0.5);
            }
            else
            {
                Start1 = new Point(Start.X + 3 * partX, Start.Y + partY * 0.5);
                End1 = new Point(Start.X + partX, Start.Y + 3 * partY + (partY * 0.5));
            }

            Notify(nameof(End));
            Notify(nameof(End1));
            Notify(nameof(Start1));
            Notify(nameof(Start));
        }
    }
}
