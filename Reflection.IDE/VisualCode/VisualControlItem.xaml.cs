﻿using Reflection.IDE.Common;
using Reflection.IDE.VisualCommon;

using ReflectionCore.Interface;
using ReflectionCore.Visual;
using ReflectionCore.Visual.Interface;
using ReflectionCore.Visual.Wpf;

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Base = Reflection.IDE.Common.Base;

namespace Reflection.IDE.VisualCode
{
    public partial class VisualControlItem : UserControl
    {
        private readonly Window parent = Application.Current.MainWindow;

        public VisualControlItem()
        {
            InitializeComponent();
            VisualClick.LeftClick(this, _ =>
            {
                if (DataContext is ControlItemController control)
                    control.Select?.Invoke(WpfVisualHelper.GetControlByInfo(control.Info));
            });
        }
    }

    public class ControlItemController : Base
    {
        public Action<IControl> Select { get; }

        public ControlInfoAttribute Info { get; }

        public Brush BG { get; }
        public SolidColorBrush TextColor { get; } = Brushes.White;

        public ControlItemController(ControlInfoAttribute info, Action<IControl> select)
        {
            Info = info;
            Select = select;
            BG = Helper.NodeColorToBrush(NodeColor.Default, 1);
        }

        public string Name => Info.Name;
        public string Group => Info.Group;
    }
}
