﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode.Controls
{
    public partial class PinTextBox : UserControl
    {
        public PinTextBox(double width, Func<string, bool> canChange = null)
        {
            InitializeComponent();
            TB.Width = width;

            if (canChange != null)
                TB.PreviewTextInput += (s, e) =>
                {
                    if (string.IsNullOrEmpty(e.Text) || (e.Text == "-" && TB.CaretIndex == 0))
                        e.Handled = false;
                    else
                        e.Handled = canChange(TB.Text.Insert(TB.CaretIndex, e.Text));
                };
        }

        private void TBKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                DependencyObject ancestor = TB.Parent;
                while (ancestor != null)
                {
                    if (ancestor is UIElement element && element.Focusable)
                    {
                        element.Focus();
                        break;
                    }

                    ancestor = VisualTreeHelper.GetParent(ancestor);
                }
                Keyboard.ClearFocus();
            }
        }
    }
}
