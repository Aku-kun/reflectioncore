﻿using LangSupport;

using ReflectionCore.Visual;
using ReflectionCore.Visual.Interface;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

using Base = Reflection.IDE.Common.Base;

namespace Reflection.IDE.VisualCode
{
    public partial class SelectControl : UserControl
    {
        public Action<IControl> OnSelect { get; set; }

        public SelectControl()
        {
            InitializeComponent();
            MaterialDesignThemes.Wpf.HintAssist.SetHint(Search, "search".Translate());

            DataContext = new SelectControlController(n => OnSelect?.Invoke(n));
        }
    }

    public class SelectControlController : Base
    {
        public SelectControlController(Action<IControl> action)
        {
            Controls = VisualHelper.GetControls().Select(n => new ControlItemController(n, action)).ToList();
        }

        public List<ControlItemController> Controls { get; }
        public IEnumerable<ControlItemController> FilterControls =>
            Controls.Where(n => string.IsNullOrEmpty(Filter) ? true : n.Name.ToLower().Contains(Filter));

        private string filter;
        public string Filter
        {
            get => filter;
            set
            {
                if (!string.Equals(value.ToLower(), filter, StringComparison.OrdinalIgnoreCase))
                {
                    filter = value.ToLower();
                    Notify(nameof(FilterControls));
                }
            }
        }
    }
}
