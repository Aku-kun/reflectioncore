﻿using Reflection.IDE.Common;
using Reflection.IDE.Interface;
using Reflection.IDE.VisualCommon;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Reflection.IDE.VisualCode
{
    public partial class NodesView : BaseUserControl
    {
        private IVisualCode Controller { get; set; }

        public NodesView()
        {
            InitializeComponent();
            
            VisualClick.LeftClick(this, e =>
            {
                if (e is VisualNode visual && visual.DataContext is IVisualNode node)
                    Controller?.LeftClick(node);
            });
            VisualClick.RightClick(this, _ =>
            {
                Controller?.RightClick();
            });
        }

        protected override void OnDataContextChange()
        {
            base.OnDataContextChange();

            if (DataContext is IVisualCode code)
            {
                Controller = code;
                Controller.Viewer = View;
                Controller.MoveOn = () => MouseMove += CodeCanvasMouseMove;
                Controller.MoveOff = () => MouseMove -= CodeCanvasMouseMove;
                Controller.Init();
            }
        }

        private void CanvasMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Point position = e.GetPosition(View);
            MatrixTransform transform = View.RenderTransform as MatrixTransform;
            Matrix matrix = transform.Matrix;
            double scale = e.Delta >= 0 ? 1.1 : (1.0 / 1.1);

            matrix.ScaleAtPrepend(scale, scale, position.X, position.Y);
            transform.Matrix = matrix;
        }

        private void CodeCanvasMouseMove(object sender, MouseEventArgs e)
            => Controller?.Move(e);

        private Point clickPosition;

        private void UserControlMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            => clickPosition = e.GetPosition(this);

        private void UserControlMouseRightButtonDown(object sender, MouseButtonEventArgs e)
            => clickPosition = e.GetPosition(this);

        private void UserControlMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (clickPosition == e.GetPosition(this))
            {
                clickPosition = new Point();
                Controller?.LeftClick((e.OriginalSource as VisualNode)?.DataContext as IVisualNode);
            }
        }

        private void UserControlMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (clickPosition == e.GetPosition(this))
            {
                clickPosition = new Point();
                Controller?.RightClick();
            }
        }
    }
}
