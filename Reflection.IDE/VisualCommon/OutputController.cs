﻿using Log;

using MaterialDesignThemes.Wpf;

using ReflectionCore.Visual.Wpf;

using System;
using System.Collections.ObjectModel;

namespace Reflection.IDE.VisualCommon
{
    public struct Message
    {
        public bool IsVisible { get; }

        public string Data { get; }
        public PackIconKind Kind { get; }

        public string ActionTitle { get; }
        public SimpleCommand ActionCommand { get; }

        public Message(string message, PackIconKind kind, string actionTitle = null, SimpleCommand action = null)
        {
            IsVisible = action != null;

            Data = message;
            Kind = kind;

            ActionTitle = actionTitle;
            ActionCommand = action;
        }
    }

    public class OutputController
    {
        public static PackIconKind ErrorIcon
            = PackIconKind.CloseBox;

        public static PackIconKind InfoIcon
            = PackIconKind.WarningBox;

        public static PackIconKind MessageIcon
            = PackIconKind.CheckboxMarked;

        public static SimpleCommand ClearHistory { get; }
            = new SimpleCommand(Clear, () => History.Count > 0);

        public static ObservableCollection<Message> History { get; }
            = new ObservableCollection<Message>();

        private static void SetOption(string message, string option, PackIconKind kind)
        {
            message = ConcatString(message, option);
            Logger.Log($"OUT - [{kind}]: {message}", ignoreEvent: true);

            History.Add(new Message(message, kind));
        }

        private static void SetOption(string message, string option, PackIconKind kind, string actionTitle, SimpleCommand action)
        {
            message = ConcatString(message, option);
            Logger.Log($"OUT - [{kind}]: {message}", ignoreEvent: true);

            History.Add(new Message(message, kind, actionTitle, action));
        }

        private static void SetOption(string message, PackIconKind kind)
            => SetOption(message, null, kind);

        private static void SetOption(string message, PackIconKind kind, string actionTitle, SimpleCommand action)
            => SetOption(message, null, kind, actionTitle, action);

        public static void Error(string message)
            => SetOption(message, ErrorIcon);

        public static void Error(string message, string option)
            => SetOption(message, option, ErrorIcon);

        public static void Error(string message, string actionTitle, SimpleCommand action)
            => SetOption(message, ErrorIcon, actionTitle, action);

        public static void Error(string message, string option, string actionTitle, SimpleCommand action)
            => SetOption(message, option, ErrorIcon, actionTitle, action);

        public static void Info(string message)
            => SetOption(message, InfoIcon);

        public static void Info(string message, string option)
            => SetOption(message, option, InfoIcon);

        public static void Info(string message, string actionTitle, SimpleCommand action)
            => SetOption(message, InfoIcon, actionTitle, action);

        public static void Info(string message, string option, string actionTitle, SimpleCommand action)
            => SetOption(message, option, InfoIcon, actionTitle, action);

        public static void Message(string message)
            => SetOption(message, MessageIcon);

        public static void Message(string message, string option)
            => SetOption(message, option, MessageIcon);

        public static void Message(string message, string actionTitle, SimpleCommand action)
            => SetOption(message, MessageIcon, actionTitle, action);

        public static void Message(string message, string option, string actionTitle, SimpleCommand action)
            => SetOption(message, option, MessageIcon, actionTitle, action);

        private static void Clear() 
            => History.Clear();

        private static string ConcatString(string string1, string string2)
            => !string.IsNullOrEmpty(string1) && !string.IsNullOrEmpty(string2)
                ? $"{string1}\n{string2}"
                : !string.IsNullOrEmpty(string1)
                    ? string1
                    : string2;
    }
}
