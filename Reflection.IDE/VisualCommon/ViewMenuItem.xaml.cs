﻿using Reflection.IDE.Interface;

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Reflection.IDE.VisualCommon
{
    public partial class ViewMenuItem : UserControl
    {
        public ViewMenuItem()
        {
            InitializeComponent();
            VisualClick.LeftClick(Container, _ =>
            {
                if (DataContext is IMenuItem menuItem && !menuItem.Select)
                    menuItem.Click?.Invoke();
            });
        }

        private void StartEdit(object sender, RoutedEventArgs e)
        {
            if (DataContext is IMenuItem menuItem)
            {
                menuItem.NewName = menuItem.Name;

                Label.Visibility = Visibility.Collapsed;
                Edit.Visibility = Visibility.Visible;
                Dispatcher.BeginInvoke(DispatcherPriority.Input,
                    new Action(() =>
                    {
                        Edit.Focus();
                        Keyboard.Focus(Edit);
                        Edit.CaretIndex = menuItem.NewName.Length;
                    }));
            }
        }

        private void LabelMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DataContext is IMenuItem menuItem && !menuItem.Select)
                menuItem.Click?.Invoke();
        }

        private void EditKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && DataContext is IMenuItem menuItem)
                OnEdit(menuItem);
        }

        private void EditLostFocus(object sender, RoutedEventArgs e)
        {
            if (DataContext is IMenuItem menuItem)
                OnEdit(menuItem);
        }

        private void OnEdit(IMenuItem menuItem)
        {
            if (string.IsNullOrEmpty(menuItem.NewName))
                return;

            Keyboard.ClearFocus();

            if (menuItem.NewName == menuItem.Name || menuItem.OnEdit?.Invoke(menuItem) == true)
            {
                menuItem.NewName = "";
                Edit.Visibility = Visibility.Collapsed;
                Label.Visibility = Visibility.Visible;
            }
            else
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Input,
                    new Action(() =>
                    {
                        Edit.Focus();
                        Keyboard.Focus(Edit);
                        Edit.CaretIndex = menuItem.NewName.Length;
                    }));
            }
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {

            if (DataContext is IMenuItem menuItem && !menuItem.Select)
                menuItem.Click?.Invoke();
        }
    }
}
