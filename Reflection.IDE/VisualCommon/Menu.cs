﻿using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;

using ReflectionCore.Visual.Wpf;

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Reflection.IDE.VisualCommon
{
    public class Menu : Base, IMenu
    {
        public bool IsReadOnly { get; set; }

        public event Action<IMenuItem> OnSelect;
        public event Action<IMenuItem> OnRemove;
        public Func<string, bool> OnAdd { get; }

        public Menu(PackIconKind icon, string name, string addHint, Func<string, bool> onAdd)
        {
            Icon = icon;
            Name = name;
            Hint = addHint;
            OnAdd = onAdd;

            Items = new ObservableCollection<IMenuItem>();
        }

        public string Name { get; }
        public string Hint { get; }
        public PackIconKind Icon { get; }
        public ObservableCollection<IMenuItem> Items { get; set; }

        public void AddItem(IMenuItem item)
        {
            item.HideEdit = IsReadOnly;
            item.HideRemove = IsReadOnly;

            item.Click = () => OnSelect?.Invoke(item);
            item.Remove = new SimpleCommand(() => OnRemove?.Invoke(item));

            Items.Add(item);
            if (!IsReadOnly)
                SortSource(item);
        }

        public void RemoveItem(IMenuItem item) => Items.Remove(item);

        private void SortSource(IMenuItem item)
        {
            int oldIndex = Items.IndexOf(item);
            int newIndex = Items.OrderBy(i => i.Name).ToList().IndexOf(item);

            Items.Move(oldIndex, newIndex);
        }
    }

    public class MenuItem : Base, IMenuItem
    {
        public bool HideEdit { get; set; }
        public bool HideRemove { get; set; }

        public Func<IMenuItem, bool> OnEdit { get; }

        public MenuItem(PackIconKind icon, string name, Func<IMenuItem, bool> onEdit)
        {
            Icon = icon;
            Name = name;
            OnEdit = onEdit;
        }

        public object Content { get; set; }
        public object Context { get; set; }

        public PackIconKind Icon { get; }
        
        private string name;
        public string Name 
        {
            get => name;
            set 
            {
                name = value;
                Notify();
            }
        }
        private string newName;
        public string NewName
        {
            get => newName;
            set
            {
                newName = value;
                Notify();
            }
        }

        public Action Click { get; set; }
        public ICommand Remove { get; set; }

        public bool Select
        {
            get => Visibility == Visibility.Visible;
            set
            {
                Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                Notify(nameof(Visibility));
            }
        }

        public Visibility Visibility { get; set; } = Visibility.Collapsed;
    }
}
