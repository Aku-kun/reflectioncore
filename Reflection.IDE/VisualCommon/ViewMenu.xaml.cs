﻿using Reflection.IDE.Interface;

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Reflection.IDE.VisualCommon
{
    public partial class ViewMenu : UserControl
    {
        public ViewMenu() => InitializeComponent();

        private void AddClick(object sender, RoutedEventArgs e)
        {
            AddBtn.IsEnabled = false;
            Input.Visibility = Visibility.Visible; 

            Dispatcher.BeginInvoke(DispatcherPriority.Input,
                new Action(() =>
                {
                    Input.Focus();
                    Keyboard.Focus(Input);
                }));
        }

        private void InputKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && DataContext is IMenu menu)
                OnInput(menu);
        }

        private void InputLostFocus(object sender, RoutedEventArgs e)
        {
            if (DataContext is IMenu menu)
                OnInput(menu);
        }

        private void OnInput(IMenu menu)
        {
            Keyboard.ClearFocus();
            AddBtn.IsEnabled = true;
            Input.Visibility = Visibility.Collapsed;

            if (string.IsNullOrEmpty(Input.Text))
                return;

            if (menu.OnAdd?.Invoke(Input.Text) == true)
                Input.Text = "";
        }
    }
}
