﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Reflection.IDE.VisualCommon
{
    public class BooleanToVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool v = (bool)value;

            if (parameter != null)
                v = !v;

            return v ? Visibility.Collapsed : (object)Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => false;
    }
}
