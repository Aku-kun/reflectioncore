﻿using Reflection.IDE.Interface;

using System.Windows;
using System.Windows.Controls;

namespace Reflection.IDE.VisualCommon
{
    public partial class SwitchV : UserControl
    {
        public SwitchV() => InitializeComponent();

        private void UserControlDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is IMenuItem item)
            {
                Content = item.Content;
                if (Content is UserControl control)
                    control.DataContext = item.Context;
            }
        }
    }
}
