﻿using Config;

using LangSupport;
using MahApps.Metro.Controls.Dialogs;
using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;

using ReflectionCore.Visual.Wpf;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Reflection.IDE
{
    public class SettingsConfigView : SettingsConfig
    {
        public SimpleCommand Close { get; set; }
        public SimpleCommand Open { get; set; }

        public int GroupIndex
            => LastUpdate == null || LastUpdate.Date == DateTime.Now.Date ? 1 : (int)Math.Round((DateTime.Now.Date - LastUpdate.Date).TotalDays + 1);
        
        public string LastUpdateDisplay
            => LastUpdate == null || LastUpdate.Date == DateTime.Now.Date
                ? "today".Translate()
                : LastUpdate.ToString("d");
        
        public string DateDisplay
            => LastUpdate == null
                ? "today".Translate()
                : LastUpdate.ToString("g");
    }

    public partial class StartMenu
    {
        public static StartMenu Instane { get; private set; }

        public StartMenu()
        {
            Instane = this;

            InitializeComponent();
            HintAssist.SetHint(Search, "search".Translate());

            DataContext = new StartMenuController();
            
        }

        public async void ShowAlert(string message)
        {
        }
    }

    public class StartMenuController : Base
    {
        public StartMenuController()
        {
            Projects = ProjectHelper.GetProjects().Select(s =>
            {
                SettingsConfigView sv = new SettingsConfigView
                {
                    ProjectName = s.ProjectName,
                    ProjectPath = s.ProjectPath,
                    LastUpdate = s.LastUpdate
                };

                sv.Close = new SimpleCommand(() => Remove(sv));
                sv.Open = new SimpleCommand(() => Open(sv.ProjectPath));

                return sv;
            }).OrderByDescending(p => p.LastUpdate).ToList();

            Actions.Add(new CodeAction("new_prolect".Translate(), PackIconKind.FilePlus, () =>
            {
                ProjectHelper.SelectFolder(p =>
                {
                    ProjectHelper.SaveProject(p);
                    Open(p);
                });
            })); 
            Actions.Add(new CodeAction("open_prject".Translate(), PackIconKind.FolderOpen, () =>
            {
                ProjectHelper.SelectFolder(p => Open(p));
            }));
        }

        public ObservableCollection<ICodeAction> Actions { get; } = new ObservableCollection<ICodeAction>();

        public List<SettingsConfigView> Projects { get; }
        public IEnumerable<SettingsConfigView> FilterProjects =>
            Projects.Where(s => string.IsNullOrEmpty(Filter) ? true : s.ProjectName.ToLower().Contains(Filter));

        private string filter;
        public string Filter
        {
            get => filter;
            set
            {
                if (!string.Equals(value.ToLower(), filter, StringComparison.OrdinalIgnoreCase))
                {
                    filter = value.ToLower();
                    Notify(nameof(FilterProjects));
                }
            }
        }

        private void Open(string path)
        {
            if (ProjectHelper.IsProject(path))
            {
                Window window = new MainWindow(path);
                StartMenu.Instane.Close();
                window.Show();
            }
            else
                StartMenu.Instane.ShowAlert("project_load_error".Translate());
        }

        private void Remove(SettingsConfigView settings)
        {
            ProjectHelper.Projects.Paths.Remove(settings.ProjectPath);
            Projects.Remove(settings);
            ProjectHelper.SaveProjects();

            Notify(nameof(FilterProjects));
        }
    }
}
