﻿using Config;

using Log;

using MahApps.Metro;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.VisualCommon;
using RSerializer.Common;

using System.Windows;
using System.Windows.Media;

using VisualHelper = ReflectionCore.Visual.VisualHelper;

namespace Reflection.IDE
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            MainController.Config = ProjectConfig.Load(SerializerHelper.Load(".rc"));
            LangSupport.LangConfig.Instane = MainController.Config.Lang;

            Color primaryColor = VisualHelper.FromArgb(MainController.Config.Visual.PrimaryColor).ToMediaColor();
            Color secondaryColor = VisualHelper.FromArgb(MainController.Config.Visual.SecondaryColor).ToMediaColor();

            IBaseTheme baseTheme = Theme.Light;
            string themeName = "BaseLight";

            if (MainController.Config.Visual.IsDark)
            {
                themeName = "BaseDark";
                baseTheme = Theme.Dark;
            }

            ITheme theme = Theme.Create(baseTheme, primaryColor, secondaryColor);
            new PaletteHelper().SetTheme(theme);
            
            ThemeManager.ChangeAppStyle(this,
                ThemeManager.GetAccent("Steel"),
                ThemeManager.GetAppTheme(themeName)
            );
            base.OnStartup(e);

            Logger.OnLog += (l, ex) => OutputController.Info(l.ToString(), ex?.Message);
        }

        private void StartupApp(object sender, StartupEventArgs e)
        {
            Window start = new StartMenu();
            start.Show();
        }
    }
}
