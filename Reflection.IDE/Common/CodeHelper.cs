﻿using Reflection.IDE.Interface;

using ReflectionCore.Interface;
using ReflectionCore.Model;

using RSerializer.Common;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Reflection.IDE.Common
{
    public static partial class Helper
    {
        public static Block GetBlock(List<IVisualNode> nodes) => GetBlock(nodes, GetNode);
        public static Block GetSaveBlock(List<IVisualNode> nodes) => GetBlock(nodes, GetSaveNode);

        private static Block GetBlock(List<IVisualNode> nodes, Func<IVisualNode, IBlockNode> getNode)
        {
            Block block = new Block();

            foreach (IVisualNode node in nodes)
                block.Items.Add(getNode(node));

            for (int i = 0; i < block.Items.Count; ++i)
            {
                BlockNode blockNode = block[i] as BlockNode;
                IVisualNode node = nodes[i];

                if (node.ThreadOut.Lines.FirstOrDefault()?.EndPin?.Node != null)
                {
                    if (blockNode.NextNodes == null)
                        blockNode.NextNodes = new List<IBlockNode>();

                    (blockNode.NextNodes as List<IBlockNode>).Add(block[nodes.IndexOf(node.ThreadOut.Lines.First().EndPin.Node)]);
                    if (node.Node.Out.Type == PinType.Thread && node.Out.FirstOrDefault()?.Lines.FirstOrDefault()?.EndPin?.Node != null)
                        (blockNode.NextNodes as List<IBlockNode>).Add(block[nodes.IndexOf(node.Out.First().Lines.First().EndPin.Node)]);
                }

                List<IBlockNodePin> inBlocks = new List<IBlockNodePin>();
                if (blockNode.In?.Count() > 0)
                    inBlocks.AddRange(blockNode.In.ToList());

                if (node.In?.Count > 0)
                    foreach (IVisualPin pin in node.In)
                    {
                        if (pin.Lines?.Count() > 0)
                            inBlocks.Add(new BlockNodePin
                            {
                                ToPin = pin.Pin,
                                FromPin = pin.Lines.First().StartPin.Pin,
                                BlockNode = block[nodes.IndexOf(pin.Lines.First().StartPin.Node)]
                            });
                        else
                            inBlocks.Add(new BlockNodePin
                            {
                                ToPin = pin.Pin
                            });
                    }

                if (node.Out?.Count > 0)
                    foreach (IVisualPin pin in node.Out)
                        if (pin.Pin.Type == PinType.Thread 
                            && pin.Lines?.Count() > 0
                            && pin.Lines.First().EndPin == pin.Lines.First().EndPin.Node.ThreadIn)
                        {
                            IBlockNode toNode = block[nodes.IndexOf(pin.Lines.First().EndPin.Node)];
                            if (toNode.In == null)
                                (toNode as BlockNode).In = new List<IBlockNodePin>();

                            (toNode.In as List<IBlockNodePin>).Add(new BlockNodePin
                            {
                                FromPin = pin.Pin,
                                ToPin = pin.Lines.First().EndPin.Pin,
                                BlockNode = blockNode
                            });
                        }

                blockNode.In = inBlocks;
            }

            block.Init();
            return block;
        }

        private static IBlockNode GetNode(IVisualNode visualNode)
        {
            BlockNode blockNode = new BlockNode
            {
                Node = visualNode.Node is ICustomNode ? MainController.GetCustomFunc(visualNode.Node.Name) : visualNode.Node
            };

            if (visualNode.Node.Type == NodeType.StaticParam || visualNode.Node.Type == NodeType.InputParam)
                blockNode.Value = visualNode.Out.FirstOrDefault()?.Value;

            return blockNode;
        }

        private static IBlockNode GetSaveNode(IVisualNode visualNode)
        {
            SaveBlockNode blockNode = new SaveBlockNode
            {
                Node = visualNode.Node is ICustomNode ? MainController.GetCustomFunc(visualNode.Node.Name) : visualNode.Node,
                X = visualNode.Drag?.Transform?.X ?? visualNode.X,
                Y = visualNode.Drag?.Transform?.Y ?? visualNode.Y,
            };

            if (visualNode.Node.Type == NodeType.StaticParam || visualNode.Node.Type == NodeType.InputParam)
                blockNode.Value = visualNode.Out.FirstOrDefault()?.Value;

            return blockNode;
        }
    }

    [RSerializable]
    public class SaveBlockNode : BlockNode
    {
        public SaveBlockNode() { }

        public double X { get; set; }
        public double Y { get; set; }
    }
}
