﻿using Reflection.IDE.Interface;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace Reflection.IDE.Common
{

    public class BaseUserControl : UserControl
    {
        public BaseUserControl()
            => DataContextChanged += (s, e) => OnDataContextChange();

        protected virtual void OnDataContextChange()
        {
            if (DataContext != null && DataContext is IVisual visual)
                visual.Control = this;
        }
    }

    public class BaseController : Base, IVisual
    {
        public FrameworkElement Control { get; set; }
        public FrameworkElement Parent { get; set; }
    }

    public class Base : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify([CallerMemberName]string name = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
