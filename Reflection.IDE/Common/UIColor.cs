﻿using ReflectionCore.Visual.ColorSet;

using System.Windows.Media;

using DColor = System.Drawing.Color;
using MColor = System.Windows.Media.Color;

namespace Reflection.IDE.Common
{
    /// <summary>
    /// *Set contains all material colors 
    /// https://www.materialpalette.com/colors
    /// </summary>
    public static class UIColor
    {
        public static Amber AmberSet { get; } = new Amber();
        public static Blue BlueSet { get; } = new Blue();
        public static BlueGrey BlueGreySet { get; } = new BlueGrey();
        public static Brown BrownSet { get; } = new Brown();
        public static Cyan CyanSet { get; } = new Cyan();
        public static DeepOrange DeepOrangeSet { get; } = new DeepOrange();
        public static DeepPurple DeepPurpleSet { get; } = new DeepPurple();
        public static Green GreenSet { get; } = new Green();
        public static Grey GreySet { get; } = new Grey();
        public static Indigo IndigoSet { get; } = new Indigo();
        public static LightBlue LightBlueSet { get; } = new LightBlue();
        public static LightGreen LightGreenSet { get; } = new LightGreen();
        public static Lime LimeSet { get; } = new Lime();
        public static Orange OrangeSet { get; } = new Orange();
        public static Pink PinkSet { get; } = new Pink();
        public static Purple PurpleSet { get; } = new Purple();
        public static Red RedSet { get; } = new Red();
        public static Teal TealSet { get; } = new Teal();
        public static Yellow YellowSet { get; } = new Yellow();

        public static SolidColorBrush ToBrush(this DColor color)
            => new SolidColorBrush(color.ToMediaColor());

        public static MColor ToMediaColor(this DColor color)
            => MColor.FromArgb(color.A, color.R, color.G, color.B);

        public static MColor ToMediaColor(this DColor color, byte a)
            => MColor.FromArgb(a, color.R, color.G, color.B);
    }
}
