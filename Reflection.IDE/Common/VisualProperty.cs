﻿using LangSupport;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Interface;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Nodes;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Reflection.IDE.Common
{
    public class VisualProperty : Base, IVisualProperty
    {
        public string Name { get; }
        public IObject Source { get; }

        public VisualProperty(IObject control, string name)
        {
            Source = control;
            Name = name;
        }

        public int Index { get; set; }
        public string Group { get; set; }

        public bool TopHeader { get; set; }
        public FrameworkElement Control { get; set; }

        public virtual object Value
        {
            get => Source.GetValue(Name);
            set => Source.SetValue(Name, value);
        }
    }

    public class ObjectProperty : VisualProperty
    {
        public ObjectProperty(IObject control, string name) : base(control, name) { }

        public override object Value { get; set; }
    }

    public class EnumProperty : VisualProperty
    {
        public EnumProperty(IObject control, string name, Type type) : base(control, name) 
            => Items = Enum.GetValues(type);

        public object Items { get; }
    }

    public class CollectionProperty : ObjectProperty
    {
        public CollectionProperty(IObject control, string name) : base(control, name)
        {
            Actions.Add(new CodeAction("add".Translate(), PackIconKind.Add, () =>
            {
                IEnumerable items = (IEnumerable)Source.GetValue(Name);
                Type type = Source.GetType().GetProperty(Name).PropertyType;

                if (type.IsGenericType && type.GenericTypeArguments.Length == 1)
                {
                    List<object> values = new List<object>();

                    foreach (object item in items)
                        values.Add(item);

                    Type itemType = type.GenericTypeArguments[0];
                    values.Add(Activator.CreateInstance(itemType));

                    Type enumerableType = typeof(Enumerable);
                    MethodInfo castMethod = enumerableType.GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(itemType);
                    MethodInfo toListMethod = enumerableType.GetMethod(nameof(Enumerable.ToList)).MakeGenericMethod(itemType);

                    object castedItems = castMethod.Invoke(null, new[] { values });
                    Source.SetValue(Name, toListMethod.Invoke(null, new[] { castedItems }));

                    Update();
                }
            }, null, 16, 11));

            Actions.Add(new CodeAction("remove".Translate(), PackIconKind.Close, () =>
            {
                IEnumerable items = (IEnumerable)Source.GetValue(Name);
                Type type = Source.GetType().GetProperty(Name).PropertyType;

                if (items == null)
                    return;

                if (type.IsGenericType && type.GenericTypeArguments.Length == 1)
                {
                    List<object> values = new List<object>();

                    foreach (object item in items)
                        values.Add(item);

                    if (values.Count <= 0)
                        return;

                    values.RemoveAt(values.Count - 1);

                    Type itemType = type.GenericTypeArguments[0];
                    Type enumerableType = typeof(Enumerable);

                    MethodInfo castMethod = enumerableType.GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(itemType);
                    MethodInfo toListMethod = enumerableType.GetMethod(nameof(Enumerable.ToList)).MakeGenericMethod(itemType);

                    object castedItems = castMethod.Invoke(null, new[] { values });
                    Source.SetValue(Name, toListMethod.Invoke(null, new[] { castedItems }));

                    Update();
                }
            }, null, 16, 11));

            Update();
        }

        public ObservableCollection<ICodeAction> Actions { get; } = new ObservableCollection<ICodeAction>();

        private void Update()
        {
            IEnumerable items = (IEnumerable)Source.GetValue(Name);
            Type type = Source.GetType().GetProperty(Name).PropertyType;

            if (items == null)
                return;

            Type itemType = type.GenericTypeArguments[0];
            PinType pinType = NodeHelper.GetPinType(itemType);

            if (pinType != PinType.IObject)
                throw new NotImplementedException();

            List<IVisualProperty> values = new List<IVisualProperty>();

            int count = 0;
            foreach (object item in items)
                values.Add(Helper.GetPropertyControl((IObject)item, new InPin(pinType, $"[{count++}]"), true));

            Value = values;
            Notify(nameof(Value));
        }
    }
}
