﻿using MaterialDesignThemes.Wpf;

using Reflection.IDE.Interface;

using ReflectionCore.Visual.Wpf;

using System;
using System.Windows;
using System.Windows.Input;

namespace Reflection.IDE.Common
{
    public class ControlAction : CodeAction
    {
        public ControlAction(PackIconKind icon, Action action, Func<bool> canAction = null, double size = 25, double fontSize = 16)
            : base(icon, action, canAction, size, fontSize) { }

        public ControlAction(string name, PackIconKind icon, Action action, Func<bool> canAction = null, double size = 25, double fontSize = 16)
            : base(name, icon, action, canAction, size, fontSize) { }

        public bool Select
        {
            get => Visibility == Visibility.Visible;
            set
            {
                Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                Notify(nameof(Visibility));
            }
        }

        public Visibility Visibility { get; set; } = Visibility.Collapsed;
    }

    public class CodeAction : Base, ICodeAction
    {
        public CodeAction(PackIconKind icon, Action action, Func<bool> canAction = null, double size = 25, double fontSize = 16)
        {
            Size = size;
            FontSize = fontSize;

            Icon = icon;
            Command = new SimpleCommand(action, canAction);
        }

        public CodeAction(string name, PackIconKind icon, Action action, Func<bool> canAction = null, double size = 25, double fontSize = 16)
            : this(icon, action, canAction, size, fontSize) => Name = name;

        public string Name { get; set; }

        public double Size { get; }
        public double FontSize { get; }

        public PackIconKind Icon { get; }
        public ICommand Command { get; }
    }
}
