﻿using LangSupport;

using Reflection.IDE.Interface;
using Reflection.IDE.VisualCode.Properties;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Visual.Interface;

using System;
using System.Linq;
using System.Reflection;

namespace Reflection.IDE.Common
{
    public static partial class Helper
    {
        private readonly static string[] IControlProperties
            = typeof(IControl)
                .GetProperties()
                .Select(p => p.Name)
                .ToArray();
        private readonly static string[] IControlElementProperties 
            = typeof(IControlElement)
                .GetProperties()
                .Select(p => p.Name)
                .Where(n => !IControlProperties.Contains(n))
                .ToArray();

        public static IVisualProperty GetPropertyControl(IObject obj, IPin pin, bool collectionItem = false)
        {
            Type type = null;
            if (!collectionItem)
                type = obj.GetType().GetProperty(pin.Name).PropertyType;
            
            IVisualProperty property = null;

            switch (pin.Type)
            {
                case PinType.Collection:
                    property = new CollectionProperty(obj, pin.Name);
                    break;
                case PinType.IObject:
                    property = new ObjectProperty(obj, pin.Name);
                    break;
                case PinType.Object:
                    property = type.IsEnum 
                        ? new EnumProperty(obj, pin.Name, type) 
                        : new VisualProperty(obj, pin.Name);
                    break;
                default:
                    property = new VisualProperty(obj, pin.Name);
                    break;
            }

            if (IControlProperties.Contains(pin.Name))
            {
                property.Index = (obj.GetType().GetProperty(pin.Name).GetCustomAttribute<ObjectPropertyAttribute>()?.Index ?? 0) + 1;
                property.Group = "main".Translate();
            }
            else if (IControlElementProperties.Contains(pin.Name))
            {
                property.Index = (obj.GetType().GetProperty(pin.Name).GetCustomAttribute<ObjectPropertyAttribute>()?.Index ?? 0) + 2;
                property.Group = "general".Translate();
            }
            else
            {
                property.Index = collectionItem ? 3 : (obj.GetType().GetProperty(pin.Name).GetCustomAttribute<ObjectPropertyAttribute>()?.Index ?? 0) + 3;
                property.Group = "other".Translate();
            }

            switch (pin.Type)
            {
                case PinType.Number:
                    property.Control = new InputPropertyVisual(v => !double.TryParse(v, out double _));
                    break;
                case PinType.Collection:
                    property.TopHeader = true;
                    property.Control = new CollectionPropertyVisual();
                    break;
                case PinType.IObject:
                    property.TopHeader = true;
                    IObject objProperty = obj;

                    if (!collectionItem)
                    {
                        objProperty = (IObject)obj.GetValue(pin.Name);

                        if (objProperty == null)
                        {
                            if (type.IsInterface)
                                throw new InvalidCastException();

                            objProperty = (IObject)Activator.CreateInstance(type);
                            obj.SetValue(pin.Name, objProperty);
                        }
                    }

                    property.Value = objProperty.Properties
                        .Select(p => GetPropertyControl(objProperty, p))
                        .OrderBy(p => p.Index)
                        .ToList();
                    property.Control = new ObjectPropertyVisual();
                    break;
                case PinType.Bool:
                    property.Control = new BoolPropertyVisual();
                    break;
                case PinType.Object:
                    if (type.IsEnum)
                        property.Control = new EnumPropertyVisual();
                    break;
                default:
                case PinType.String:
                    property.Control = new InputPropertyVisual();
                    break;
            }

            return property;
        }
    }
}
