﻿using Config;

using LangSupport;

using Reflection.IDE.VisualCommon;

using RSerializer;
using RSerializer.Common;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Reflection.IDE.Common
{
    public static class ProjectHelper
    {
        static ProjectHelper()
            => Projects = new AllProject();

        [RSerializable]
        public class AllProject
        {
            public AllProject()
                => Paths = new List<string>();

            public List<string> Paths { get; set; } 
        }

        public static AllProject Projects { get; private set; }

        public static List<SettingsConfig> GetProjects()
        {
            try
            {
                List<SettingsConfig> result = new List<SettingsConfig>();

                using (Serializer serializer = new Serializer())
                {
                    Projects = serializer.Deserialize<AllProject>(File.ReadAllText(SettingsConfig.PROJECTS_PATH));
                    foreach (string path in Projects.Paths)
                    {
                        if (IsProject(path))
                            result.Add(serializer.Deserialize<SettingsConfig>(File.ReadAllText(Path.Combine(path, SettingsConfig.CONFIG))));
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return new List<SettingsConfig>();
            }
        }

        public static void SaveProjects()
        {
            using (Serializer serializer = new Serializer())
            {
                SerializerHelper.Save(serializer.Serialize(Projects), SettingsConfig.PROJECTS_PATH);
            }
        }

        public static void SelectFolder(Action<string> callback)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    callback?.Invoke(fbd.SelectedPath);
            }
        }

        public static bool IsProject(string path)
        {
            bool result = false;

            if (Directory.Exists(path)
                && File.Exists(Path.Combine(path, SettingsConfig.CONFIG))
                && Directory.Exists(Path.Combine(path, SettingsConfig.RESOURCES)))
                result = true;

            return result;
        }

        public static bool LoadProject(string path)
        {
            try
            {
                MainController.ProjectSettings = new SettingsConfig();
                MainController.ProjectConfig = new ProjectConfig();

                using (Serializer serializer = new Serializer())
                {
                    MainController.ProjectSettings = serializer.Deserialize<SettingsConfig>(
                        File.ReadAllText(Path.Combine(path, SettingsConfig.CONFIG)));
                }

                if (MainController.ProjectSettings == null)
                    throw new ArgumentNullException(nameof(MainController.ProjectSettings));

                using (Serializer serializer = new Serializer())
                {
                    MainController.ProjectConfig = serializer.Deserialize<ProjectConfig>(
                        File.ReadAllText(Path.Combine(path, $"{MainController.ProjectSettings.ProjectName}{SettingsConfig.CONFIG}")));
                }

                if (MainController.ProjectConfig == null)
                    throw new ArgumentNullException(nameof(MainController.ProjectConfig));

                OutputController.Message("project_load".Translate());
                return true;
            }
            catch (Exception ex)
            {
                MainController.ProjectSettings = new SettingsConfig();
                MainController.ProjectConfig = new ProjectConfig();

                OutputController.Error("project_load_error".Translate(), ex.Message);
            }

            return false;
        }

        public static void SaveProject(string path)
        {
            try
            {
                MainController.ProjectSettings.ProjectPath = path;
                MainController.ProjectSettings.LastUpdate = DateTime.Now;

                if (!Projects.Paths.Contains(path))
                {
                    Projects.Paths.Add(path);
                    SaveProjects();
                }

                string p = Path.Combine(path, SettingsConfig.RESOURCES);

                if (!Directory.Exists(p))
                    Directory.CreateDirectory(p);

                using (Serializer serializer = new Serializer())
                {
                    p = Path.Combine(path, SettingsConfig.CONFIG);
                    SerializerHelper.Save(serializer.Serialize(MainController.ProjectSettings), p);
                    File.SetAttributes(p, File.GetAttributes(p) | FileAttributes.Hidden);
                }

                using (Serializer serializer = new Serializer())
                {
                    SerializerHelper.Save(serializer.Serialize(MainController.ProjectConfig),
                        Path.Combine(path, $"{MainController.ProjectSettings.ProjectName}{SettingsConfig.CONFIG}"));
                }

                OutputController.Message("project_save".Translate());
            }
            catch (Exception ex)
            {
                OutputController.Error("project_save_error".Translate(), ex.Message);
            }
        }

        public static void Compile()
        {
            OutputController.Message("build".Translate());

            void Complite()
            {
                MainController.Instane.Save(MainController.ProjectSettings.ProjectPath, false);
                ApplyClient();
            }

            if (MainController.ProjectSettings == null || string.IsNullOrEmpty(MainController.ProjectSettings?.ProjectPath))
                SelectFolder(p =>
                {
                    if (MainController.ProjectSettings == null)
                        MainController.ProjectSettings = new SettingsConfig();

                    MainController.ProjectSettings.ProjectPath = p;
                    Complite();
                });
            else
                Complite();
        }

        private static void ApplyClient()
        {
            try
            {
                string bin = Path.Combine(MainController.ProjectSettings.ProjectPath, SettingsConfig.BIN);
                string path = Path.Combine(bin, $"{MainController.ProjectSettings.ProjectName}{SettingsConfig.EXE}");

                //Change META

                if (!Directory.Exists(bin))
                    Directory.CreateDirectory(bin);

                File.Copy(SettingsConfig.CLIENT_PATH, path, true);

                using (Serializer serializer = new Serializer())
                {
                    SerializerHelper.Save(serializer.Serialize(MainController.ProjectConfig),
                        Path.Combine(bin, $"{MainController.ProjectSettings.ProjectName}{SettingsConfig.CONFIG}"));
                }

                OutputController.Message("build_complite".Translate());

                Debug(path);
            }
            catch (Exception ex)
            {
                OutputController.Error("project_build_error".Translate(), ex.Message);
            }
        }

        private static void Debug(string path)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                FileName = path,
                WindowStyle = ProcessWindowStyle.Minimized
            };

            try
            {
                using (Process debug = Process.Start(startInfo))
                {
                    debug.WaitForExit();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
