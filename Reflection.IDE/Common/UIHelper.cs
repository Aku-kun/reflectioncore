﻿using Reflection.IDE.VisualCode.Controls;

using ReflectionCore.Interface;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Reflection.IDE.Common
{
    public static partial class Helper
    {
        public static int GetNodeWidth(INode node)
        {
            switch (node.Type)
            {
                case NodeType.StaticParam:
                    switch (node.Out.Type)
                    {
                        case PinType.Bool:
                            return 80;
                        case PinType.Number:
                            return 140;
                        default:
                            return 210;
                    }
                case NodeType.InputParam:
                    return 210;
                case NodeType.Start:
                case NodeType.End:
                    return 100;
                default:
                    return 210;
            }
        }

        public static UserControl GetPrevControl(IPin pin, bool isInput = false)
        {
            if (isInput) 
                return new PinTextBox(168);

            if (pin.Type == PinType.IObject && pin is IOutPin)
                return new PinLabel();

            if (pin is IThreadPin threadPin && threadPin.IsVisible && !string.IsNullOrEmpty(threadPin.Name))
                return new PinLabel();

            if (pin is IStaticPin)
            {
                switch (pin.Type)
                {
                    case PinType.Bool:
                        return new PinCheckBox();
                    case PinType.Number:
                        return new PinTextBox(98, v => !double.TryParse(v, out double _));
                    case PinType.String:
                        return new PinTextBox(168);
                    default:
                        return null;
                }
            }

            if (pin is IOutPin outPin && !outPin.IsVoid)
                return new PinLabel();

            return null;
        }

        public static UserControl GetNextControl(IPin pin)
        {
            if (pin.Type == PinType.IObject && !(pin is IOutPin))
                return new PinLabel();

            if (pin is IThreadPin || pin is IStaticPin || pin is IOutPin)
                return null;

            return new PinLabel();
        }

        public static SolidColorBrush PinTypeToBrush(PinType type)
        {
            switch (type)
            {
                case PinType.Bool:
                    return new SolidColorBrush(NodeColorToColor(NodeColor.LightRed));
                case PinType.Number:
                case PinType.String:
                    return new SolidColorBrush(NodeColorToColor(NodeColor.LightBlue));
                default:
                    return Brushes.White;
            }
        }

        public static Brush NodeColorToBrush(NodeColor nodeColor, double offset = 0.3)
            => GetGradientBrush(
                NodeColorToColor(nodeColor, 200),
                UIColor.GreySet.Color800.ToMediaColor(200),
                offset
            );

        public static Color NodeColorToColor(NodeColor nodeColor, byte alpha = 255)
        {
            switch (nodeColor)
            {
                default:
                case NodeColor.Default:
                    return UIColor.GreySet.Color600.ToMediaColor(alpha);
                case NodeColor.Red:
                    return UIColor.RedSet.Color900.ToMediaColor(alpha);
                case NodeColor.LightRed:
                    return UIColor.RedSet.Color300.ToMediaColor(alpha);
                case NodeColor.Blue:
                    return UIColor.IndigoSet.Color900.ToMediaColor(alpha);
                case NodeColor.LightBlue:
                    return UIColor.IndigoSet.Color400.ToMediaColor(alpha);
                case NodeColor.Green:
                    return UIColor.TealSet.Color700.ToMediaColor(alpha);
                case NodeColor.LightGreen:
                    return UIColor.TealSet.ColorA400.ToMediaColor(alpha);
                case NodeColor.Orange:
                    return UIColor.OrangeSet.ColorA400.ToMediaColor(alpha);
            }
        }

        private static LinearGradientBrush GetGradientBrush(Color start, Color end, double offset = 0.3)
            => new LinearGradientBrush
            {
                StartPoint = new Point(0, 0),
                EndPoint = new Point(0, 1),
                GradientStops =
                {
                    new GradientStop(start, 0),
                    new GradientStop(end, offset),
                    new GradientStop(end, 1),
                }
            };
    }
}
