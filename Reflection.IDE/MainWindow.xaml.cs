﻿using Config;

using LangSupport;

using MaterialDesignThemes.Wpf;

using Reflection.IDE.Common;
using Reflection.IDE.Interface;
using Reflection.IDE.VisualCode;
using Reflection.IDE.VisualCommon;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Nodes;
using ReflectionCore.Visual;
using ReflectionCore.Visual.Interface;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Base = Reflection.IDE.Common.Base;

namespace Reflection.IDE
{
    public partial class MainWindow
    {
        public static event Action<string> OnCustomFuncAdd;
        public static void AddFunc(string name) => OnCustomFuncAdd?.Invoke(name);

        public static event Action<string> OnCustomFuncUpdate;
        public static void UpdateFunc(string name) => OnCustomFuncUpdate?.Invoke(name);

        public static event Action<string, string> OnCustomFuncNameChange;
        public static void FuncNameChange(string oldName, string newName) => OnCustomFuncNameChange?.Invoke(oldName, newName);

        public static event Action<string> OnCustomFuncRemove;
        public static void RemoveFunc(string name) => OnCustomFuncRemove?.Invoke(name);

        public static event Action<string> OnPageAdd;
        public static void AddPage(string name) => OnPageAdd?.Invoke(name);

        public static event Action<string, string> OnPageNameChange;
        public static void PageNameChange(string oldName, string newName) => OnPageNameChange?.Invoke(oldName, newName);

        public static event Action<string> OnPageRemove;
        public static void RemovePage(string name) => OnPageRemove?.Invoke(name);

        private static MainWindow instane;

        public MainWindow()
        {
            InitializeComponent();
            instane = this;

            DataContext = new MainController();
            HistoryLabel.Text = "history".Translate();
        }

        public MainWindow(string path) : this()
        {
            MainController.Instane.Load(path);
        }

        private static readonly SelectNode SelectNode = new SelectNode();
        public static void ShowSelectNode(Action<INode> onSelect, bool isVisual = false)
        {
            SelectNode.Visual(isVisual);
            SelectNode.OnSelect = n =>
            {
                if (MainController.Instane?.Funcs?.Items != null)
                {
                    IMenuItem item = MainController.Instane.Funcs.Items.FirstOrDefault(i => i.Select);
                    if (item != null && item.Name == n.Name)
                    {
                        OutputController.Error("rec_error".Translate());
                        CloseAllDialog();
                    }
                    else
                    {
                        onSelect?.Invoke(n);
                        SelectNode.OnSelect = null;
                    }
                }
            };

            instane.DialogHost.DialogContent = SelectNode;
            instane.DialogHost.IsOpen = true;
        }

        private static readonly SelectControl SelectControl = new SelectControl();
        public static void ShowSelectControl(Action<IControl> onSelect)
        {
            SelectControl.OnSelect = n =>
            {
                onSelect?.Invoke(n);
                SelectControl.OnSelect = null;
            };

            instane.DialogHost.DialogContent = SelectControl;
            instane.DialogHost.IsOpen = true;
        }

        public static void CloseAllDialog()
            => instane.DialogHost.IsOpen = false;

        private void WindowKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (MainController.Instane.Context is IMenuItem menuItem && menuItem.Context is IVisualCode visual)
                visual.KeyDown(e);
        }
    }

    public class MainController : Base
    {
        internal readonly Menu General = null, Pages = null, Funcs = null;

        public static ProjectConfig Config { get; set; }

        public static ProjectConfig ProjectConfig { get; set; } = new ProjectConfig();
        public static SettingsConfig ProjectSettings { get; set; } = new SettingsConfig();

        public static MainController Instane { get; private set; }

        public MainController()
        {
            General = new Menu(PackIconKind.Settings, "general".Translate(), "", null)
            {
                IsReadOnly = true
            };
            General.OnSelect += OnMenuSelect;
            SetGeneral();

            Pages = new Menu(PackIconKind.FileDocumentBoxMultipleOutline, "pages".Translate(), "name".Translate(), OnPageAdd);
            Pages.OnRemove += OnPageItemRemove;
            Pages.OnSelect += OnMenuSelect;

            Funcs = new Menu(PackIconKind.Cogs, "funcs".Translate(), "name".Translate(), OnFuncAdd);
            Funcs.OnRemove += OnFuncItemRemove;
            Funcs.OnSelect += OnMenuSelect;

            Menus.Add(General);
            Menus.Add(Pages);
            Menus.Add(Funcs);

            Instane = this;

            OutputController.Message("Hellow word!");
        }

        private void SetGeneral()
        {
            General.AddItem(new MenuItem(PackIconKind.SettingsApplications, "settings".Translate(), null)
            {
                Context = new ProjectController(),
                Content = new ProjectPage()
            });

            General.AddItem(new MenuItem(PackIconKind.Language, "langs".Translate(), null)
            {
                Context = null,
                Content = null
            });

            General.AddItem(new MenuItem(PackIconKind.ArrowDownBoldBoxOutline, "save".Translate(), null)
            {
                Context = null,
                Content = null
            });
            General.Items.Last().Click = () =>
            {
                if (ProjectSettings == null || string.IsNullOrEmpty(ProjectSettings?.ProjectPath))
                    ProjectHelper.SelectFolder(p =>
                    {
                        if (ProjectSettings == null)
                            ProjectSettings = new SettingsConfig();

                        ProjectSettings.ProjectPath = p;
                        Save(ProjectSettings.ProjectPath);
                    });
                else
                    Save(ProjectSettings.ProjectPath);
            };

            General.AddItem(new MenuItem(PackIconKind.OpenInApp, "load".Translate(), null)
            {
                Context = null,
                Content = null
            });
            General.Items.Last().Click = () =>
            {
                ProjectHelper.SelectFolder(p =>
                {
                    if (ProjectHelper.IsProject(p))
                        Load(p);
                    else
                        OutputController.Error("project_load_error".Translate());
                });
            };
        }

        public void Save(string path, bool isSave = true)
        {
            FuncsCompile(isSave);
            PagesCompile(isSave);

            ProjectConfig.Meta = FuncsIndex;
            ProjectHelper.SaveProject(path);
        }

        public void Load(string path)
        {
            if (ProjectHelper.LoadProject(path))
            {
                Dictionary<string, int> meta = ProjectConfig.Meta as Dictionary<string, int>;
                if (meta == null && ProjectConfig.Pages?.Count > 0 && ProjectConfig.Funcs?.Count > 0)
                    throw new ArgumentNullException();

                Pages.Items.Clear();
                Funcs.Items.Clear();

                if (ProjectConfig.Pages != null)
                    for (int i = 0; i < ProjectConfig.Pages.Count; ++i)
                    {
                        OnPageAdd(meta.ElementAt(i).Key);
                        (Pages.Items.Last().Context as DesignController).Load(ProjectConfig.Pages[i], ProjectConfig.Funcs[i]);
                    }
                
                if (ProjectConfig.Funcs != null)
                    for (int i = ProjectConfig.Pages?.Count ?? 0; i < ProjectConfig.Funcs.Count; ++i)
                    {
                        OnFuncAdd(meta.ElementAt(i).Key);
                        (Funcs.Items.Last().Context as CodeController).Load(ProjectConfig.Funcs[i]);
                    }
            }
        }


        public object Context { get; set; }
        private void OnMenuSelect(IMenuItem item)
        {
            foreach (IMenuItem menuItem in Pages.Items.Where(i => i.Select).Concat(Funcs.Items.Where(i => i.Select)).Concat(General.Items.Where(i => i.Select)))
                menuItem.Select = false;

            item.Select = true;

            Context = item;
            Notify(nameof(Context));
        }

        public ObservableCollection<IMenu> Menus { get; } = new ObservableCollection<IMenu>();
        public OutputController Output { get; } = new OutputController();
        private IEnumerable<string> Check 
            => NodeHelper.AllNodes.Select(n => n.Name.ToLower())
                .Concat(Pages.Items.Select(i => i.Name.ToLower()))
                .Concat(Funcs.Items.Select(i => i.Name.ToLower()));

        private bool OnPageAdd(string name)
        {
            int index = 1;
            string uName = name;
            while (Check.Any(n => n == uName.ToLower()))
            {
                uName = $"{name}#{index}";
                index++;
            }
            name = uName;

            IMenuItem menuItem = new MenuItem(PackIconKind.FileDocumentBoxOutline, name, OnPageEdit)
            {
                Context = new DesignController(new List<INode> { NodeHelper.GetNode(nameof(NodeHelper.End)) }),
                Content = new DesignCanvas()
            };

            Pages.AddItem(menuItem);
            OnMenuSelect(menuItem);

            MainWindow.AddPage(name);

            return true;
        }

        private bool OnPageEdit(IMenuItem item)
        {
            if (Check.Any(n => n == item.NewName.ToLower()))
            {
                OutputController.Error("name_error".Translate());
                return false;
            }

            string oldName = item.Name;
            item.Name = item.NewName;

            MainWindow.PageNameChange(oldName, item.Name);

            return true;
        }

        private void OnPageItemRemove(IMenuItem item)
        {
            if (item.Select)
                OnMenuSelect(General.Items[0]);

            Pages.RemoveItem(item);

            MainWindow.RemovePage(item.Name);
        }

        private bool OnFuncAdd(string name)
        {
            int index = 1;
            string uName = name;
            while (Check.Any(n => n == uName.ToLower()))
            {
                uName = $"{name}#{index}";
                index++;
            }
            name = uName;

            IMenuItem menuItem = new MenuItem(PackIconKind.CodeBraces, name, OnFuncEdit)
            {
                Content = new CodeCanvas()
            };

            menuItem.Context = new CodeController(NodeHelper.GetCustomMethodStart(), () =>
            {
                MainWindow.UpdateFunc(menuItem.Name);
            }, 
            () =>
            {
                MainWindow.AddFunc(name);
            });

            Funcs.AddItem(menuItem);
            OnMenuSelect(menuItem);

            return true;
        }

        private bool OnFuncEdit(IMenuItem item)
        {
            if (Check.Any(n => n == item.NewName.ToLower()))
            {
                OutputController.Error("name_error".Translate());
                return false;
            }

            string oldName = item.Name;
            item.Name = item.NewName;

            MainWindow.FuncNameChange(oldName, item.Name);

            return true;
        }

        private void OnFuncItemRemove(IMenuItem item)
        {
            if (item.Select)
                OnMenuSelect(General.Items[0]);

            Funcs.RemoveItem(item);

            MainWindow.RemoveFunc(item.Name);
        }

        private static Dictionary<string, int> FuncsIndex { get; } = new Dictionary<string, int>();
        public static void FuncsCompile(bool isSave = true)
        {
            FuncsIndex.Clear();
            ProjectConfig.Funcs?.Clear();

            if (Instane?.Funcs?.Items != null && Instane?.Pages?.Items != null)
            {
                IEnumerable<IMenuItem> items = Instane.Pages.Items.Concat(Instane.Funcs.Items);

                int index = 0;
                foreach (IMenuItem item in items)
                {
                    if (!isSave)
                        OutputController.Message("build_start".Translate() + $": func `{item.Name}`");
                    
                    FuncsIndex.Add(item.Name, index);
                    ProjectConfig.Funcs.Add(null);
                    index++;
                }

                foreach (IMenuItem item in items)
                {
                    CodeController code = (item.Context as CodeController);
                    if (code != null)
                    {
                        CompileFunc(item.Name, code, isSave);

                        if (!isSave)
                            OutputController.Message("build_end".Translate() + $": func `{item.Name}`");
                    }
                }
            }
        }

        public static void PagesCompile(bool isSave = true)
        {
            ProjectConfig.Pages?.Clear();

            if (Instane?.Pages?.Items != null)
            {
                foreach (IMenuItem item in Instane.Pages.Items)
                {
                    if (!isSave)
                        OutputController.Message("build_start".Translate() + $": page `{item.Name}`");

                    DesignController design = (item.Context as DesignController);
                    if (design != null)
                    {
                        IPage page = VisualHelper.GetBase(design.Page);
                        IBlock block = ProjectConfig.Funcs[FuncsIndex[item.Name]];

                        if (block != null)
                        {
                            void setBlockNode(IControl element)
                            {
                                foreach (IPin pin in element.Pins)
                                    if (pin.Type == PinType.Thread)
                                        element.SetValue(pin.Name, block.Items.FirstOrDefault(bn =>
                                            bn.In.Any(i => i.BlockNode?.Node?.Name == element.Name && i.FromPin?.Name == pin.Name)
                                        ));
                            }

                            setBlockNode(page);

                            foreach (IControlElement element in page.Elements)
                                setBlockNode(element);
                        }

                        ProjectConfig.Pages.Add(page);

                        if (!isSave)
                            OutputController.Message("build_end".Translate() + $": page `{item.Name}`");
                    }
                }
            }
        }

        private static void CompileFunc(string name, CodeController code, bool isSave)
        {
            int index = FuncsIndex[name];
            if (ProjectConfig.Funcs[index] == null)
            {
                IBlock block = isSave
                    ? Helper.GetSaveBlock(code.Nodes.ToList())
                    : Helper.GetBlock(code.Nodes.ToList());
                block.Id = index + 1;
                ProjectConfig.Funcs[index] = block;
            }
        }

        public static INode GetCustomFunc(string name)
        {
            int index = FuncsIndex[name];
            return new CustomNode(name, ProjectConfig.Funcs[index]);
        }

        public static INode GetCustomFuncView(string name)
        {
            if (Instane?.Funcs?.Items != null)
            {
                IMenuItem item = Instane.Funcs.Items.FirstOrDefault(i => i.Name == name);
                if (item != null)
                {
                    CodeController code = (item.Context as CodeController);
                    if (code != null)
                    {
                        IVisualNode end = code.Nodes.FirstOrDefault(n => n.Node.Type == NodeType.End || NodeHelper.IsReturn(n.Node));
                        return new CustomNode
                        {
                            Name = name,

                            Color = NodeColor.Green,
                            Type = NodeType.CustomFunc,

                            ThreadIn = new ThreadPin(null, true),
                            ThreadOut = new ThreadPin(null, true),

                            In = code.Nodes
                                .Where(n => n.Node.Type == NodeType.InputParam)
                                .Select(n => (IPin)new InPin(n.Node.Out.Type, n.Out.FirstOrDefault()?.Value?.ToString()))
                                .ToList(),
                            Out = new OutPin(PinType.Object, end.Node.Name, !end.In.Any(p => p.Lines.Any()))
                        };
                    }
                }
            }

            throw new NullReferenceException();
        }
    }
}
