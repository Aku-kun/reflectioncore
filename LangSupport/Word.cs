﻿using RSerializer.Common;

using System.Collections.Generic;

namespace LangSupport
{
    [RSerializable]
    public class Word
    {
        public string Key { get; set; }
        public Dictionary<int, string> Values { get; set; }
    }
}
