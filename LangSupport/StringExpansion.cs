﻿namespace LangSupport
{
    public static class StringExpansion
    {
        public static string Translate(this string str)
        {
            if (!string.IsNullOrEmpty(str))
                return LangConfig.Instane.GetValue(str) ?? str;

            return str;
        }
    }
}
