﻿using RSerializer.Common;

namespace LangSupport
{
    [RSerializable]
    public class Lang : IReference
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
