﻿using ReflectionCore.Visual.Interface;

using System;
using System.Linq;
using System.Reflection;

namespace ReflectionCore.Visual.Wpf
{
    public static class WpfVisualHelper
    {
        public static IControl GetControlByInfo(ControlInfoAttribute info)
        {
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in a.GetTypes())
                {
                    if (t.BaseType == info.Type)
                    {
                        IControl control = (IControl)Activator.CreateInstance(t);
                        if (control != null)
                        {
                            control.Name = info.Name;
                            return control;
                        }
                    }
                }
            }

            return null;
        }

        public static IPage GetView(IPage page)
        {
            VisualPage result = new VisualPage();

            if (result.Control != null)
            {
                foreach (string name in page.Properties.Select(p => p.Name).Concat(page.Pins.Select(p => p.Name)))
                    if (name != nameof(IControl.This))
                        result.SetValue(name, page.GetValue(name));

                if (page.Elements != null)
                {
                    foreach (IControlElement element in page.Elements)
                    {
                        Type elementType = element.GetType();
                        foreach (Type t in Assembly.GetAssembly(typeof(VisualPage)).GetTypes())
                        {
                            if (t.BaseType == elementType)
                            {
                                IControlElement control = (IControlElement)Activator.CreateInstance(t);
                                if (control?.Control != null)
                                {
                                    foreach (string name in element.Properties.Select(p => p.Name).Concat(element.Pins.Select(p => p.Name)))
                                        if (name != nameof(IControl.This))
                                            control.SetValue(name, element.GetValue(name));

                                    result.Elements.Add(control);
                                }
                            }
                        }
                    }

                    result.UpdateElements();
                }
            }

            return result;
        }
    }
}
