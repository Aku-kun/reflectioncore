﻿using LangSupport;

using System.Windows.Controls;

namespace ReflectionCore.Visual.Wpf
{
    public class VisualButton : ButtonControl
    {
        public VisualButton() 
            => source = new Button();

        private readonly Button source;
        public override object Control => source;

        public override string Title 
        {
            get => base.Title;
            set
            {
                base.Title = value;

                source.Content = Translate ? value.Translate() : value;
            }
        }

        public override bool Translate 
        { 
            get => base.Translate;
            set
            {
                base.Translate = value;

                Title = Title;
            }
        }

        public override int Column
        {
            get => base.Column;
            set
            {
                base.Column = value;

                if (value >= 0)
                    Grid.SetColumn(source, value);
            }
        }

        public override int Row 
        { 
            get => base.Row;
            set
            {
                base.Row = value;

                if (value >= 0)
                    Grid.SetRow(source, value);
            }
        }

        public override int ColumnSpan
        {
            get => base.ColumnSpan;
            set
            {
                base.ColumnSpan = value;

                if (value > 0)
                    Grid.SetColumnSpan(source, value);
            }
        }

        public override int RowSpan
        {
            get => base.RowSpan;
            set
            {
                base.RowSpan = value;

                if (value > 0)
                    Grid.SetRowSpan(source, value);
            }
        }

        public override double Width 
        { 
            get => base.Width;
            set
            {
                base.Width = value;
                source.Width = value == 0 ? double.NaN : value;
            }
        }

        public override double Height
        {
            get => base.Height;
            set
            {
                base.Height = value;
                source.Height = value == 0 ? double.NaN : value;
            }
        }

        public override bool IsVisible 
        {
            get => base.IsVisible;
            set
            {
                base.IsVisible = value;

                source.Visibility = value
                    ? System.Windows.Visibility.Visible
                    : System.Windows.Visibility.Collapsed;
            }
        }

        public override bool IsEnable 
        {
            get => source.IsEnabled;
            set => source.IsEnabled = value;
        }

        public override VisualMargin Margin
        {
            get => base.Margin;
            set
            {
                base.Margin = value;

                if (value != null)
                {
                    value.OnChange -= MarginChanged;
                    value.OnChange += MarginChanged;
                }
            }
        }

        public override VisualMargin Padding 
        {
            get => base.Padding;
            set
            {
                base.Padding = value;

                if (value != null)
                {
                    value.OnChange -= PaddingChanged;
                    value.OnChange += PaddingChanged;
                }
            }
        }

        private void MarginChanged(string obj)
        {
            if (source == null)
                return;

            source.Margin = new System.Windows.Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
        }

        private void PaddingChanged(string obj)
        {
            if (source == null)
                return;

            source.Padding = new System.Windows.Thickness(Padding.Left, Padding.Top, Padding.Right, Padding.Bottom);
        }

        public override Alignment VerticalAlignment
        {
            get => base.VerticalAlignment;
            set
            {
                base.VerticalAlignment = value;

                switch (value)
                {
                    case Alignment.Start:
                        source.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        break;
                    case Alignment.Center:
                        source.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                        break;
                    case Alignment.End:
                        source.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                        break;
                    case Alignment.Stretch:
                        source.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                        break;
                }
            }
        }

        public override Alignment HorizontalAlignment 
        {
            get => base.HorizontalAlignment;
            set
            {
                base.HorizontalAlignment = value;

                switch (value)
                {
                    case Alignment.Start:
                        source.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        break;
                    case Alignment.Center:
                        source.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        break;
                    case Alignment.End:
                        source.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                        break;
                    case Alignment.Stretch:
                        source.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        break;
                }
            }
        }

        public override void Show() => IsVisible = true;
        public override void Hide() => IsVisible = false;

        public override void Enable() => IsEnable = true;
        public override void Disable() => IsEnable = false;
    }
}
