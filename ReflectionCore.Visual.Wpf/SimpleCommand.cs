﻿using System;
using System.Windows.Input;

namespace ReflectionCore.Visual.Wpf
{
    public class SimpleCommand : ICommand
    {
        private Action action;
        private Func<bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public SimpleCommand(Action action, Func<bool> canExecute = null)
        {
            this.action = action;
            this.canExecute = canExecute;
        }

        public void NewAction(Action newAction)
            => action = newAction;

        public void NewCanExecute(Func<bool> newCanExecute)
            => canExecute = newCanExecute;

        public bool CanExecute(object parameter)
            => canExecute == null || canExecute();

        public void Execute(object parameter)
            => action();
    }

    public class RelayCommand<T> : ICommand
    {
        private Action<T> action;
        private Func<T, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<T> action, Func<T, bool> canExecute = null)
        {
            this.action = action;
            this.canExecute = canExecute;
        }

        public void NewAction(Action<T> newAction)
            => action = newAction;

        public void NewCanExecute(Func<T, bool> newCanExecute)
            => canExecute = newCanExecute;

        public bool CanExecute(object parameter)
            => canExecute == null || canExecute((T)parameter);

        public void Execute(object parameter)
            => action((T)parameter);
    }
}
