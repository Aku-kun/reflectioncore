﻿using LangSupport;

using ReflectionCore.Visual.Interface;

using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ReflectionCore.Visual.Wpf
{
    public class VisualPage : PageControl, INotifyPropertyChanged
    {
        public override bool IsDev 
        {
            get => base.IsDev;
            set
            {
                base.IsDev = value;
                source.ShowGridLines = value;
            }
        }

        public VisualPage()
            => source = new Grid
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Background = Brushes.Transparent
            };

        private readonly Grid source;

        public override object Control => source;

        public override List<IControlElement> Elements 
        {
            get => base.Elements;
            set
            {
                base.Elements = value;
                UpdateElements();
            }
        }

        public override void UpdateElements()
        {
            source.Children.Clear();

            foreach (IControlElement element in Elements)
            {
                if (!(element.Control is UIElement uI))
                    continue;

                source.Children.Add(uI);
            }
        }

        public override List<VisualColumn> Columns 
        { 
            get => base.Columns;
            set
            {
                base.Columns = value;

                source.ColumnDefinitions.Clear();

                foreach (VisualColumn column in Columns)
                {
                    column.OnChange -= ColumnOnChange;
                    column.OnChange += ColumnOnChange;

                    ColumnDefinition definition = new ColumnDefinition
                    {
                        MaxWidth = column.MaxWidth <= 0 ? double.PositiveInfinity : column.MaxWidth
                    };

                    definition.Width = column.Width < 0
                        ? new GridLength(1, GridUnitType.Auto)
                        : column.Width == 0 
                            ? new GridLength(1, GridUnitType.Star) 
                            : new GridLength(column.Width);

                    if (column.MaxWidth > 0)
                        definition.MaxWidth = column.MaxWidth;

                    source.ColumnDefinitions.Add(definition);
                }
            }
        }

        private void ColumnOnChange(string obj)
        {
            for (int i = 0; i < Columns.Count; ++i)
            {
                source.ColumnDefinitions[i].Width = Columns[i].Width < 0
                        ? new GridLength(1, GridUnitType.Auto)
                        : Columns[i].Width == 0
                            ? new GridLength(1, GridUnitType.Star)
                            : new GridLength(Columns[i].Width);

                source.ColumnDefinitions[i].MaxWidth = Columns[i].MaxWidth <= 0 ? double.PositiveInfinity : Columns[i].MaxWidth;
            }
        }

        public override List<VisualRow> Rows 
        { 
            get => base.Rows;
            set
            {
                base.Rows = value;

                source.RowDefinitions.Clear();

                foreach (VisualRow row in Rows)
                {
                    row.OnChange -= RowOnChange;
                    row.OnChange += RowOnChange;

                    RowDefinition definition = new RowDefinition
                    {
                        MaxHeight = row.MaxHeight <= 0 ? double.PositiveInfinity : row.MaxHeight
                    };

                    definition.Height = row.Height < 0
                            ? new GridLength(1, GridUnitType.Auto)
                            : row.Height == 0
                                ? new GridLength(1, GridUnitType.Star)
                                : new GridLength(row.Height);

                    source.RowDefinitions.Add(definition);
                }
            }
        }

        private void RowOnChange(string obj)
        {
            for (int i = 0; i < Rows.Count; ++i)
            {
                source.RowDefinitions[i].Height = Rows[i].Height < 0
                               ? new GridLength(1, GridUnitType.Auto)
                               : Rows[i].Height == 0
                                   ? new GridLength(1, GridUnitType.Star)
                                   : new GridLength(Rows[i].Height);

                source.RowDefinitions[i].MaxHeight = Rows[i].MaxHeight <= 0 ? double.PositiveInfinity : Rows[i].MaxHeight;
            }
        }

        public override string Title
        {
            get => base.Title;
            set => base.Title = Translate ? value.Translate() : value;
        }

        public override bool Translate
        {
            get => base.Translate;
            set
            {
                base.Translate = value;

                Title = Title;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public SimpleCommand Select => new SimpleCommand(() => Manager.OnSelect(this));
        public override bool IsSelect 
        {
            get => base.IsSelect;
            set
            {
                base.IsSelect = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSelect)));
            }
        }
    }
}
