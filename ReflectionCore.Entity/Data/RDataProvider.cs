﻿using ReflectionCore.Entity.Core.Data;
using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Data
{
    public class RDataProvider : IDataProvider
    {
        private RContext _context;
        private static RDataProvider instance;
        
        public static RDataProvider Instance 
        { 
            get
            {
                if (instance == null)
                    instance = new RDataProvider();

                return instance;
            }  
        }

        public object Execute(IAction[] actions)
        {
            List<object> results = new List<object>();
            foreach (IAction action in actions)
            {
                switch (action.Action)
                {
                    case Core.Actions.Get:
                        {
                            results.Add(_context.Get(action.Models));
                            break;
                        }
                    case Core.Actions.Set:
                        {
                            results.Add(_context.Set(action.Models));
                            break;
                        }
                    case Core.Actions.Delete:
                        {
                            results.Add(_context.Delete(action.Models));
                            break;
                        }
                    case Core.Actions.Update:
                        {
                            results.Add(_context.Update(action.Models));
                            break;
                        }
                    case Core.Actions.Create:
                        {
                            results.Add(_context.Create(action.Models));
                            break;
                        }
                    case Core.Actions.Alter:
                        {
                            results.Add(_context.Alter(action.Models));
                            break;
                        }
                    case Core.Actions.Drop:
                        {
                            results.Add(_context.Drop(action.Models));
                            break;
                        }
                }
            }

            return results;
        }

        public void Init(DBConfig config, ISQLManager sqlManager)
        {
            _context = new RContext(config, sqlManager);
        }
    }
}
