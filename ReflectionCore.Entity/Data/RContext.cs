﻿using ReflectionCore.Entity.Core.Data;
using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Data
{
    class RContext : ContextBase
    {
        public RContext(DBConfig config, ISQLManager sqlManager) 
            : base(config, sqlManager) 
        {
        }

        public override object Alter(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Create(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Delete(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Drop(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Get(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Set(IModel[] models)
        {
            throw new NotImplementedException();
        }

        public override object Update(IModel[] models)
        {
            throw new NotImplementedException();
        }
    }
}
