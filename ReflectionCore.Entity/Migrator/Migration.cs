﻿using Log;
using ReflectionCore.Entity.Core.Data;
using ReflectionCore.Entity.Core.Migrator;
using RSerializer;
using RSerializer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Migrator
{
    [RSerializable]
    internal class Migration : MigrationBase
    {
        public Migration(IModel entity)
            : base(entity)
        {
        }

        public override string Apply()
        {
            try
            {
                using (Serializer serializer = new Serializer())
                {
                    return serializer.Serialize(this);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("какая-то ошибка пусть будет   ", ex);
            }

            return default;
        }
    }
}
