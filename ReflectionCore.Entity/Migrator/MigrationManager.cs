﻿using ReflectionCore.Entity.Core.Migrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Migrator
{
    internal class MigrationManager : IMigrationManager
    {
        public IList<Migration> Migrations { get; set; }

        public MigrationManager()
        {
            Migrations = new List<Migration>();
        }

        public void Load()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            foreach (Migration migration in Migrations)
            {
                string xml = migration.Apply();
            }
        }
    }
}
