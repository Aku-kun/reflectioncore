﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

using RSerializer.Common;

namespace ReflectionCore.Visual
{
    [ControlInfo("Button", "")]
    [RSerializable]
    public class ButtonControl : BaseElement, IButtonMethod
    {
        [ObjectProperty(1)]
        public virtual string Title { get; set; }
        [ObjectProperty(2)]
        public virtual bool Translate { get; set; } = true;
        [ObjectProperty(3)]
        public virtual VisualMargin Margin { get; set; }
        [ObjectProperty(4)]
        public virtual VisualMargin Padding { get; set; }
        [ObjectProperty(5)]
        public virtual Alignment VerticalAlignment { get; set; } = Alignment.Center;
        [ObjectProperty(6)]
        public virtual Alignment HorizontalAlignment { get; set; } = Alignment.Stretch;
        [ObjectProperty(7)]
        [ObjectPin]
        public virtual bool IsVisible { get; set; } = true;
        [ObjectProperty(8)]
        [ObjectPin]
        public virtual bool IsEnable { get; set; } = true;

        [ObjectPin]
        [Reference]
        public IBlockNode Action { get; set; }

        public virtual void Disable() { }
        public virtual void Enable() { }
        public virtual void Hide() { }
        public virtual void Show() { }
    }
}
