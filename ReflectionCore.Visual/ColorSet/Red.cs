﻿using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Red
    {
        public Color Color50 => ColorTranslator.FromHtml("#ffebee");
        public Color Color100 => ColorTranslator.FromHtml("#ffcdd2");
        public Color Color200 => ColorTranslator.FromHtml("#ef9a9a");
        public Color Color300 => ColorTranslator.FromHtml("#e57373");
        public Color Color400 => ColorTranslator.FromHtml("#ef5350");
        public Color Color500 => ColorTranslator.FromHtml("#f44336");
        public Color Color600 => ColorTranslator.FromHtml("#e53935");
        public Color Color700 => ColorTranslator.FromHtml("#d32f2f");
        public Color Color800 => ColorTranslator.FromHtml("#c62828");
        public Color Color900 => ColorTranslator.FromHtml("#b71c1c");
        public Color ColorA100 => ColorTranslator.FromHtml("#ff8a80");
        public Color ColorA200 => ColorTranslator.FromHtml("#ff5252");
        public Color ColorA400 => ColorTranslator.FromHtml("#ff1744");
        public Color ColorA700 => ColorTranslator.FromHtml("#d50000");
    }
}
