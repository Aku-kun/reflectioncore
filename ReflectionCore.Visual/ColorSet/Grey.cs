using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Grey
    {
        public Color Color50 => ColorTranslator.FromHtml("#fafafa");
        public Color Color100 => ColorTranslator.FromHtml("#f5f5f5");
        public Color Color200 => ColorTranslator.FromHtml("#eeeeee");
        public Color Color300 => ColorTranslator.FromHtml("#e0e0e0");
        public Color Color400 => ColorTranslator.FromHtml("#bdbdbd");
        public Color Color500 => ColorTranslator.FromHtml("#9e9e9e");
        public Color Color600 => ColorTranslator.FromHtml("#757575");
        public Color Color700 => ColorTranslator.FromHtml("#616161");
        public Color Color800 => ColorTranslator.FromHtml("#424242");
        public Color Color900 => ColorTranslator.FromHtml("#212121");
    }
}
