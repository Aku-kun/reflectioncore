using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Blue
    {
        public Color Color50 => ColorTranslator.FromHtml("#e3f2fd");
        public Color Color100 => ColorTranslator.FromHtml("#bbdefb");
        public Color Color200 => ColorTranslator.FromHtml("#90caf9");
        public Color Color300 => ColorTranslator.FromHtml("#64b5f6");
        public Color Color400 => ColorTranslator.FromHtml("#42a5f5");
        public Color Color500 => ColorTranslator.FromHtml("#2196f3");
        public Color Color600 => ColorTranslator.FromHtml("#1e88e5");
        public Color Color700 => ColorTranslator.FromHtml("#1976d2");
        public Color Color800 => ColorTranslator.FromHtml("#1565c0");
        public Color Color900 => ColorTranslator.FromHtml("#0d47a1");
        public Color ColorA100 => ColorTranslator.FromHtml("#82b1ff");
        public Color ColorA200 => ColorTranslator.FromHtml("#448aff");
        public Color ColorA400 => ColorTranslator.FromHtml("#2979ff");
        public Color ColorA700 => ColorTranslator.FromHtml("#2962ff");
    }
}
