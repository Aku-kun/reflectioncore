﻿using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Pink
    {
        public Color Color50 => ColorTranslator.FromHtml("#fce4ec");
        public Color Color100 => ColorTranslator.FromHtml("#f8bbd0");
        public Color Color200 => ColorTranslator.FromHtml("#f48fb1");
        public Color Color300 => ColorTranslator.FromHtml("#f06292");
        public Color Color400 => ColorTranslator.FromHtml("#ec407a");
        public Color Color500 => ColorTranslator.FromHtml("#e91e63");
        public Color Color600 => ColorTranslator.FromHtml("#d81b60");
        public Color Color700 => ColorTranslator.FromHtml("#c2185b");
        public Color Color800 => ColorTranslator.FromHtml("#ad1457");
        public Color Color900 => ColorTranslator.FromHtml("#880e4f");
        public Color ColorA100 => ColorTranslator.FromHtml("#ff80ab");
        public Color ColorA200 => ColorTranslator.FromHtml("#ff4081");
        public Color ColorA400 => ColorTranslator.FromHtml("#f50057");
        public Color ColorA700 => ColorTranslator.FromHtml("#c51162");
    }
}
