using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class DeepOrange
    {
        public Color Color50 => ColorTranslator.FromHtml("#fbe9e7");
        public Color Color100 => ColorTranslator.FromHtml("#ffccbc");
        public Color Color200 => ColorTranslator.FromHtml("#ffab91");
        public Color Color300 => ColorTranslator.FromHtml("#ff8a65");
        public Color Color400 => ColorTranslator.FromHtml("#ff7043");
        public Color Color500 => ColorTranslator.FromHtml("#ff5722");
        public Color Color600 => ColorTranslator.FromHtml("#f4511e");
        public Color Color700 => ColorTranslator.FromHtml("#e64a19");
        public Color Color800 => ColorTranslator.FromHtml("#d84315");
        public Color Color900 => ColorTranslator.FromHtml("#bf360c");
        public Color ColorA100 => ColorTranslator.FromHtml("#ff9e80");
        public Color ColorA200 => ColorTranslator.FromHtml("#ff6e40");
        public Color ColorA400 => ColorTranslator.FromHtml("#ff3d00");
        public Color ColorA700 => ColorTranslator.FromHtml("#dd2c00");
    }
}
