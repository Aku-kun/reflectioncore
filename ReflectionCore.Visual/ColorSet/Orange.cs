using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Orange
    {
        public Color Color50 => ColorTranslator.FromHtml("#fff3e0");
        public Color Color100 => ColorTranslator.FromHtml("#ffe0b2");
        public Color Color200 => ColorTranslator.FromHtml("#ffcc80");
        public Color Color300 => ColorTranslator.FromHtml("#ffb74d");
        public Color Color400 => ColorTranslator.FromHtml("#ffa726");
        public Color Color500 => ColorTranslator.FromHtml("#ff9800");
        public Color Color600 => ColorTranslator.FromHtml("#fb8c00");
        public Color Color700 => ColorTranslator.FromHtml("#f57c00");
        public Color Color800 => ColorTranslator.FromHtml("#ef6c00");
        public Color Color900 => ColorTranslator.FromHtml("#e65100");
        public Color ColorA100 => ColorTranslator.FromHtml("#ffd180");
        public Color ColorA200 => ColorTranslator.FromHtml("#ffab40");
        public Color ColorA400 => ColorTranslator.FromHtml("#ff9100");
        public Color ColorA700 => ColorTranslator.FromHtml("#ff6d00");
    }
}
