using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class BlueGrey
    {
        public Color Color50 => ColorTranslator.FromHtml("#eceff1");
        public Color Color100 => ColorTranslator.FromHtml("#cfd8dc");
        public Color Color200 => ColorTranslator.FromHtml("#b0bec5");
        public Color Color300 => ColorTranslator.FromHtml("#90a4ae");
        public Color Color400 => ColorTranslator.FromHtml("#78909c");
        public Color Color500 => ColorTranslator.FromHtml("#607d8b");
        public Color Color600 => ColorTranslator.FromHtml("#546e7a");
        public Color Color700 => ColorTranslator.FromHtml("#455a64");
        public Color Color800 => ColorTranslator.FromHtml("#37474f");
        public Color Color900 => ColorTranslator.FromHtml("#263238");
    }
}
