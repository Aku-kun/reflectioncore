using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Yellow
    {
        public Color Color50 => ColorTranslator.FromHtml("#fffde7");
        public Color Color100 => ColorTranslator.FromHtml("#fff9c4");
        public Color Color200 => ColorTranslator.FromHtml("#fff59d");
        public Color Color300 => ColorTranslator.FromHtml("#fff176");
        public Color Color400 => ColorTranslator.FromHtml("#ffee58");
        public Color Color500 => ColorTranslator.FromHtml("#ffeb3b");
        public Color Color600 => ColorTranslator.FromHtml("#fdd835");
        public Color Color700 => ColorTranslator.FromHtml("#fbc02d");
        public Color Color800 => ColorTranslator.FromHtml("#f9a825");
        public Color Color900 => ColorTranslator.FromHtml("#f57f17");
        public Color ColorA100 => ColorTranslator.FromHtml("#ffff8d");
        public Color ColorA200 => ColorTranslator.FromHtml("#ffff00");
        public Color ColorA400 => ColorTranslator.FromHtml("#ffea00");
        public Color ColorA700 => ColorTranslator.FromHtml("#ffd600");
    }
}
