using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Cyan
    {
        public Color Color50 => ColorTranslator.FromHtml("#e0f7fa");
        public Color Color100 => ColorTranslator.FromHtml("#b2ebf2");
        public Color Color200 => ColorTranslator.FromHtml("#80deea");
        public Color Color300 => ColorTranslator.FromHtml("#4dd0e1");
        public Color Color400 => ColorTranslator.FromHtml("#26c6da");
        public Color Color500 => ColorTranslator.FromHtml("#00bcd4");
        public Color Color600 => ColorTranslator.FromHtml("#00acc1");
        public Color Color700 => ColorTranslator.FromHtml("#0097a7");
        public Color Color800 => ColorTranslator.FromHtml("#00838f");
        public Color Color900 => ColorTranslator.FromHtml("#006064");
        public Color ColorA100 => ColorTranslator.FromHtml("#84ffff");
        public Color ColorA200 => ColorTranslator.FromHtml("#18ffff");
        public Color ColorA400 => ColorTranslator.FromHtml("#00e5ff");
        public Color ColorA700 => ColorTranslator.FromHtml("#00b8d4");
    }
}
