using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class DeepPurple
    {
        public Color Color50 => ColorTranslator.FromHtml("#ede7f6");
        public Color Color100 => ColorTranslator.FromHtml("#d1c4e9");
        public Color Color200 => ColorTranslator.FromHtml("#b39ddb");
        public Color Color300 => ColorTranslator.FromHtml("#9575cd");
        public Color Color400 => ColorTranslator.FromHtml("#7e57c2");
        public Color Color500 => ColorTranslator.FromHtml("#673ab7");
        public Color Color600 => ColorTranslator.FromHtml("#5e35b1");
        public Color Color700 => ColorTranslator.FromHtml("#512da8");
        public Color Color800 => ColorTranslator.FromHtml("#4527a0");
        public Color Color900 => ColorTranslator.FromHtml("#311b92");
        public Color ColorA100 => ColorTranslator.FromHtml("#b388ff");
        public Color ColorA200 => ColorTranslator.FromHtml("#7c4dff");
        public Color ColorA400 => ColorTranslator.FromHtml("#651fff");
        public Color ColorA700 => ColorTranslator.FromHtml("#6200ea");
    }
}
