using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Teal
    {
        public Color Color50 => ColorTranslator.FromHtml("#e0f2f1");
        public Color Color100 => ColorTranslator.FromHtml("#b2dfdb");
        public Color Color200 => ColorTranslator.FromHtml("#80cbc4");
        public Color Color300 => ColorTranslator.FromHtml("#4db6ac");
        public Color Color400 => ColorTranslator.FromHtml("#26a69a");
        public Color Color500 => ColorTranslator.FromHtml("#009688");
        public Color Color600 => ColorTranslator.FromHtml("#00897b");
        public Color Color700 => ColorTranslator.FromHtml("#00796b");
        public Color Color800 => ColorTranslator.FromHtml("#00695c");
        public Color Color900 => ColorTranslator.FromHtml("#004d40");
        public Color ColorA100 => ColorTranslator.FromHtml("#a7ffeb");
        public Color ColorA200 => ColorTranslator.FromHtml("#64ffda");
        public Color ColorA400 => ColorTranslator.FromHtml("#1de9b6");
        public Color ColorA700 => ColorTranslator.FromHtml("#00bfa5");
    }
}
