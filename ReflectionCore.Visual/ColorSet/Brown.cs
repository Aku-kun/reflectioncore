using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Brown
    {
        public Color Color50 => ColorTranslator.FromHtml("#efebe9");
        public Color Color100 => ColorTranslator.FromHtml("#d7ccc8");
        public Color Color200 => ColorTranslator.FromHtml("#bcaaa4");
        public Color Color300 => ColorTranslator.FromHtml("#a1887f");
        public Color Color400 => ColorTranslator.FromHtml("#8d6e63");
        public Color Color500 => ColorTranslator.FromHtml("#795548");
        public Color Color600 => ColorTranslator.FromHtml("#6d4c41");
        public Color Color700 => ColorTranslator.FromHtml("#5d4037");
        public Color Color800 => ColorTranslator.FromHtml("#4e342e");
        public Color Color900 => ColorTranslator.FromHtml("#3e2723");
    }
}
