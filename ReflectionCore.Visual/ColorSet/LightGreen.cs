using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class LightGreen
    {
        public Color Color50 => ColorTranslator.FromHtml("#f1f8e9");
        public Color Color100 => ColorTranslator.FromHtml("#dcedc8");
        public Color Color200 => ColorTranslator.FromHtml("#c5e1a5");
        public Color Color300 => ColorTranslator.FromHtml("#aed581");
        public Color Color400 => ColorTranslator.FromHtml("#9ccc65");
        public Color Color500 => ColorTranslator.FromHtml("#8bc34a");
        public Color Color600 => ColorTranslator.FromHtml("#7cb342");
        public Color Color700 => ColorTranslator.FromHtml("#689f38");
        public Color Color800 => ColorTranslator.FromHtml("#558b2f");
        public Color Color900 => ColorTranslator.FromHtml("#33691e");
        public Color ColorA100 => ColorTranslator.FromHtml("#ccff90");
        public Color ColorA200 => ColorTranslator.FromHtml("#b2ff59");
        public Color ColorA400 => ColorTranslator.FromHtml("#76ff03");
        public Color ColorA700 => ColorTranslator.FromHtml("#64dd17");
    }
}
