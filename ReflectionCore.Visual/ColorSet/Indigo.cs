using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Indigo
    {
        public Color Color50 => ColorTranslator.FromHtml("#e8eaf6");
        public Color Color100 => ColorTranslator.FromHtml("#c5cae9");
        public Color Color200 => ColorTranslator.FromHtml("#9fa8da");
        public Color Color300 => ColorTranslator.FromHtml("#7986cb");
        public Color Color400 => ColorTranslator.FromHtml("#5c6bc0");
        public Color Color500 => ColorTranslator.FromHtml("#3f51b5");
        public Color Color600 => ColorTranslator.FromHtml("#3949ab");
        public Color Color700 => ColorTranslator.FromHtml("#303f9f");
        public Color Color800 => ColorTranslator.FromHtml("#283593");
        public Color Color900 => ColorTranslator.FromHtml("#1a237e");
        public Color ColorA100 => ColorTranslator.FromHtml("#8c9eff");
        public Color ColorA200 => ColorTranslator.FromHtml("#536dfe");
        public Color ColorA400 => ColorTranslator.FromHtml("#3d5afe");
        public Color ColorA700 => ColorTranslator.FromHtml("#304ffe");
    }
}
