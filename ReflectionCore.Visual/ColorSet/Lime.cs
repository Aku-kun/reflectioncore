using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Lime
    {
        public Color Color50 => ColorTranslator.FromHtml("#f9fbe7");
        public Color Color100 => ColorTranslator.FromHtml("#f0f4c3");
        public Color Color200 => ColorTranslator.FromHtml("#e6ee9c");
        public Color Color300 => ColorTranslator.FromHtml("#dce775");
        public Color Color400 => ColorTranslator.FromHtml("#d4e157");
        public Color Color500 => ColorTranslator.FromHtml("#cddc39");
        public Color Color600 => ColorTranslator.FromHtml("#c0ca33");
        public Color Color700 => ColorTranslator.FromHtml("#afb42b");
        public Color Color800 => ColorTranslator.FromHtml("#9e9d24");
        public Color Color900 => ColorTranslator.FromHtml("#827717");
        public Color ColorA100 => ColorTranslator.FromHtml("#f4ff81");
        public Color ColorA200 => ColorTranslator.FromHtml("#eeff41");
        public Color ColorA400 => ColorTranslator.FromHtml("#c6ff00");
        public Color ColorA700 => ColorTranslator.FromHtml("#aeea00");
    }
}
