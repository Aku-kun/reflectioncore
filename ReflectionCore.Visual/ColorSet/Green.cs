using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Green
    {
        public Color Color50 => ColorTranslator.FromHtml("#e8f5e9");
        public Color Color100 => ColorTranslator.FromHtml("#c8e6c9");
        public Color Color200 => ColorTranslator.FromHtml("#a5d6a7");
        public Color Color300 => ColorTranslator.FromHtml("#81c784");
        public Color Color400 => ColorTranslator.FromHtml("#66bb6a");
        public Color Color500 => ColorTranslator.FromHtml("#4caf50");
        public Color Color600 => ColorTranslator.FromHtml("#43a047");
        public Color Color700 => ColorTranslator.FromHtml("#388e3c");
        public Color Color800 => ColorTranslator.FromHtml("#2e7d32");
        public Color Color900 => ColorTranslator.FromHtml("#1b5e20");
        public Color ColorA100 => ColorTranslator.FromHtml("#b9f6ca");
        public Color ColorA200 => ColorTranslator.FromHtml("#69f0ae");
        public Color ColorA400 => ColorTranslator.FromHtml("#00e676");
        public Color ColorA700 => ColorTranslator.FromHtml("#00c853");
    }
}
