using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class LightBlue
    {
        public Color Color50 => ColorTranslator.FromHtml("#e1f5fe");
        public Color Color100 => ColorTranslator.FromHtml("#b3e5fc");
        public Color Color200 => ColorTranslator.FromHtml("#81d4fa");
        public Color Color300 => ColorTranslator.FromHtml("#4fc3f7");
        public Color Color400 => ColorTranslator.FromHtml("#29b6f6");
        public Color Color500 => ColorTranslator.FromHtml("#03a9f4");
        public Color Color600 => ColorTranslator.FromHtml("#039be5");
        public Color Color700 => ColorTranslator.FromHtml("#0288d1");
        public Color Color800 => ColorTranslator.FromHtml("#0277bd");
        public Color Color900 => ColorTranslator.FromHtml("#01579b");
        public Color ColorA100 => ColorTranslator.FromHtml("#80d8ff");
        public Color ColorA200 => ColorTranslator.FromHtml("#40c4ff");
        public Color ColorA400 => ColorTranslator.FromHtml("#00b0ff");
        public Color ColorA700 => ColorTranslator.FromHtml("#0091ea");
    }
}
