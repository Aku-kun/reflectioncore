using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Purple
    {
        public Color Color50 => ColorTranslator.FromHtml("#f3e5f5");
        public Color Color100 => ColorTranslator.FromHtml("#e1bee7");
        public Color Color200 => ColorTranslator.FromHtml("#ce93d8");
        public Color Color300 => ColorTranslator.FromHtml("#ba68c8");
        public Color Color400 => ColorTranslator.FromHtml("#ab47bc");
        public Color Color500 => ColorTranslator.FromHtml("#9c27b0");
        public Color Color600 => ColorTranslator.FromHtml("#8e24aa");
        public Color Color700 => ColorTranslator.FromHtml("#7b1fa2");
        public Color Color800 => ColorTranslator.FromHtml("#6a1b9a");
        public Color Color900 => ColorTranslator.FromHtml("#4a148c");
        public Color ColorA100 => ColorTranslator.FromHtml("#ea80fc");
        public Color ColorA200 => ColorTranslator.FromHtml("#e040fb");
        public Color ColorA400 => ColorTranslator.FromHtml("#d500f9");
        public Color ColorA700 => ColorTranslator.FromHtml("#aa00ff");
    }
}
