using System.Drawing;

namespace ReflectionCore.Visual.ColorSet
{
    public class Amber
    {
        public Color Color50 => ColorTranslator.FromHtml("#fff8e1");
        public Color Color100 => ColorTranslator.FromHtml("#ffecb3");
        public Color Color200 => ColorTranslator.FromHtml("#ffe082");
        public Color Color300 => ColorTranslator.FromHtml("#ffd54f");
        public Color Color400 => ColorTranslator.FromHtml("#ffca28");
        public Color Color500 => ColorTranslator.FromHtml("#ffc107");
        public Color Color600 => ColorTranslator.FromHtml("#ffb300");
        public Color Color700 => ColorTranslator.FromHtml("#ffa000");
        public Color Color800 => ColorTranslator.FromHtml("#ff8f00");
        public Color Color900 => ColorTranslator.FromHtml("#ff6f00");
        public Color ColorA100 => ColorTranslator.FromHtml("#ffe57f");
        public Color ColorA200 => ColorTranslator.FromHtml("#ffd740");
        public Color ColorA400 => ColorTranslator.FromHtml("#ffc400");
        public Color ColorA700 => ColorTranslator.FromHtml("#ffab00");
    }
}
