﻿using ReflectionCore.Visual.ColorSet;
using ReflectionCore.Visual.Interface;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;

namespace ReflectionCore.Visual
{
    /// <summary>
    /// *Set contains all material colors 
    /// https://www.materialpalette.com/colors
    /// </summary>
    public static class VisualHelper
    {
        public static Amber AmberSet { get; } = new Amber();
        public static Blue BlueSet { get; } = new Blue();
        public static BlueGrey BlueGreySet { get; } = new BlueGrey();
        public static Brown BrownSet { get; } = new Brown();
        public static Cyan CyanSet { get; } = new Cyan();
        public static DeepOrange DeepOrangeSet { get; } = new DeepOrange();
        public static DeepPurple DeepPurpleSet { get; } = new DeepPurple();
        public static Green GreenSet { get; } = new Green();
        public static Grey GreySet { get; } = new Grey();
        public static Indigo IndigoSet { get; } = new Indigo();
        public static LightBlue LightBlueSet { get; } = new LightBlue();
        public static LightGreen LightGreenSet { get; } = new LightGreen();
        public static Lime LimeSet { get; } = new Lime();
        public static Orange OrangeSet { get; } = new Orange();
        public static Pink PinkSet { get; } = new Pink();
        public static Purple PurpleSet { get; } = new Purple();
        public static Red RedSet { get; } = new Red();
        public static Teal TealSet { get; } = new Teal();
        public static Yellow YellowSet { get; } = new Yellow();

        public static Color FromArgb(int color)
        {
            byte[] argbarray = BitConverter.GetBytes(color).Reverse().ToArray();
            return Color.FromArgb(argbarray[0], argbarray[1], argbarray[2], argbarray[3]);
        }

        public static List<ControlInfoAttribute> GetControls()
        {
            List<ControlInfoAttribute> result = new List<ControlInfoAttribute>();

            foreach (Type t in typeof(PageControl).Assembly.GetTypes())
            {
                ControlInfoAttribute info = t.GetCustomAttribute<ControlInfoAttribute>();
                if (info != null)
                {
                    info.Type = t;
                    result.Add(info);
                }
            }

            return result;
        }

        public static IPage GetBase(IPage page)
        {
            PageControl result = new PageControl();
            foreach (string name in page.Properties.Select(p => p.Name).Concat(page.Pins.Select(p => p.Name)))
            {
                if (name != nameof(IControl.This))
                    result.SetValue(name, page.GetValue(name));
            }

            foreach (IControlElement element in page.Elements)
            {
                Type elementType = element.GetType();
                foreach (Type t in Assembly.GetAssembly(typeof(PageControl)).GetTypes())
                {
                    if (elementType.BaseType == t)
                    {
                        IControlElement control = (IControlElement)Activator.CreateInstance(t);
                        if (control != null)
                        {
                            foreach (string name in element.Properties.Select(p => p.Name).Concat(element.Pins.Select(p => p.Name)))
                                if (name != nameof(IControl.This))
                                    control.SetValue(name, element.GetValue(name));

                            result.Elements.Add(control);
                        }
                    }
                }
            }

            return result;
        }
    }
}
