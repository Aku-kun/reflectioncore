﻿using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Visual.Interface;

using RSerializer.Common;

using System.Collections.Generic;

namespace ReflectionCore.Visual
{
    [RSerializable]
    public class PageControl : Base, IPage, IPageMethod
    {
        protected IPageManager Manager { get; private set; }
        public virtual IPage Init(IPageManager pageManager)
        {
            Manager = pageManager;
            IsSelect = false;
            return this;
        }

        public virtual bool IsSelect { get; set; }

        public virtual List<IControlElement> Elements { get; set; } = new List<IControlElement>();
        public virtual void UpdateElements() { }

        [ObjectProperty(3)]
        public virtual List<VisualColumn> Columns { get; set; } = new List<VisualColumn>();
        [ObjectProperty(4)]
        public virtual List<VisualRow> Rows { get; set; } = new List<VisualRow>();

        [ObjectPin]
        [Reference]
        public IBlockNode OnLoad { get; set; }

        [ObjectProperty(1)]
        public virtual string Title { get; set; }
        [ObjectProperty(2)]
        public virtual bool Translate { get; set; } = true;

        public virtual void ShowAlert(string message) => Manager.ShowAlert(message);
        public virtual void ShowWait() => Manager.ShowWait();
        public virtual void HideWait() => Manager.HideWait();
    }
}
