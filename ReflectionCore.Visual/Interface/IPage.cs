﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

using RSerializer.Common;

using System;
using System.Collections.Generic;

namespace ReflectionCore.Visual.Interface
{
    public interface IPageManager : IPageMethod
    {
        Action<IPage> OnSelect { get; }
    }

    public interface IPage : IControl
    {
        IPage Init(IPageManager pageManager);

        List<IControlElement> Elements { get; set; }
        void UpdateElements();

        List<VisualColumn> Columns { get; set; }
        List<VisualRow> Rows { get; set; }

        bool IsSelect { get; set; }
    }

    [RSerializable]
    public class Column : BaseChange
    {
        [ObjectProperty]
        public virtual int Width { get; set; }
        [ObjectProperty]
        public virtual int MaxWidth { get; set; }
    }

    [RSerializable]
    public class Row : BaseChange
    {
        [ObjectProperty]
        public virtual int Height { get; set; }
        [ObjectProperty]
        public virtual int MaxHeight { get; set; }
    }

    [RSerializable]
    public class VisualColumn : Column
    {
        public override int Width
        { 
            get => base.Width;
            set
            {
                base.Width = value;
                Notify();
            }
        }

        public override int MaxWidth 
        { 
            get => base.MaxWidth;
            set
            {
                base.MaxWidth = value;
                Notify();
            }
        }
    }

    [RSerializable]
    public class VisualRow : Row
    {
        public override int Height
        {
            get => base.Height;
            set
            {
                base.Height = value;
                Notify();
            }
        }

        public override int MaxHeight
        {
            get => base.MaxHeight;
            set
            {
                base.MaxHeight = value;
                Notify();
            }
        }
    }
}
