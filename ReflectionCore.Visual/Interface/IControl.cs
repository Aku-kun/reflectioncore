﻿using ReflectionCore.Interface;

using System;

namespace ReflectionCore.Visual.Interface
{
    public interface INotifyChanges
    {
        event Action<string> OnChange;
        void Notify(string name = "");
    }

    public interface IControl : IObject, INotifyChanges
    {
        bool IsDev { get; set; }

        string Name { get; set; }
        object Control { get; }

        IObject This { get; set; }
    }

    public interface IControlElement : IControl
    {
        double Width { get; set; }
        double Height { get; set; }

        int Column { get; set; }
        int Row { get; set; }

        int ColumnSpan { get; set; }
        int RowSpan { get; set; }
    }
}
