﻿using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Visual.Interface;

using RSerializer.Common;

using System;
using System.Runtime.CompilerServices;

namespace ReflectionCore.Visual
{
    [RSerializable]
    public class BaseChange : PObject, INotifyChanges
    {
        public event Action<string> OnChange;

        public void Notify([CallerMemberName]string name = "")
            => OnChange?.Invoke(name);
    }

    [RSerializable]
    public class Base : BaseChange, IControl
    {
        [Ignore]
        public virtual bool IsDev { get; set; }

        private string name;
        [ObjectProperty]
        public virtual string Name 
        {
            get => name;
            set
            {
                name = value;
                Notify();
            }
        }

        [Ignore]
        public virtual object Control { get; }

        [Ignore]
        [ObjectPin]
        public IObject This { get => this; set => throw new NotSupportedException(); }
    }

    [RSerializable]
    public class BaseElement : Base, IControlElement
    {
        [ObjectProperty(1)]
        public virtual double Width { get; set; }
        [ObjectProperty(2)]
        public virtual double Height { get; set; }
        [ObjectProperty(3)]
        public virtual int Column { get; set; }
        [ObjectProperty(4)]
        public virtual int ColumnSpan { get; set; }
        [ObjectProperty(5)]
        public virtual int Row { get; set; }
        [ObjectProperty(6)]
        public virtual int RowSpan { get; set; }
    }

    [RSerializable]
    public class Margin : BaseChange
    {
        [ObjectProperty(1)]
        public virtual int Left { get; set; }
        [ObjectProperty(2)]
        public virtual int Right { get; set; }
        [ObjectProperty(3)]
        public virtual int Top { get; set; }
        [ObjectProperty(4)]
        public virtual int Bottom { get; set; }
    }

    [RSerializable]
    public class VisualMargin : Margin
    {
        public override int Left
        {
            get => base.Left;
            set
            {
                base.Left = value;
                Notify();
            }
        }

        public override int Right
        {
            get => base.Right;
            set
            {
                base.Right = value;
                Notify();
            }
        }

        public override int Top
        {
            get => base.Top;
            set
            {
                base.Top = value;
                Notify();
            }
        }

        public override int Bottom
        {
            get => base.Bottom;
            set
            {
                base.Bottom = value;
                Notify();
            }
        }
    }


    public enum Alignment
    {
        Stretch = 0,
        Start = 1,
        Center = 2,
        End = 3
    }
}
