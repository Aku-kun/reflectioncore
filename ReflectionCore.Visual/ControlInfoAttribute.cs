﻿using System;

namespace ReflectionCore.Visual
{
    public class ControlInfoAttribute : Attribute
    {
        public string Name { get; set; }
        public string Group { get; set; }

        public Type Type { get; set; }

        public ControlInfoAttribute(string name, string group)
        {
            Name = name;
            Group = group;
        }
    }
}
