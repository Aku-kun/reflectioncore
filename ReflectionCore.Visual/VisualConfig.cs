﻿using RSerializer.Common;

namespace ReflectionCore.Visual
{
    [RSerializable]
    public class VisualConfig
    {
        public bool IsDark { get; set; }
        public int PrimaryColor { get; set; }
        public int SecondaryColor { get; set; }
    }
}
