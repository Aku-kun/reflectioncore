﻿using Log;

using ReflectionCore.Interface;
using ReflectionCore.Model;
using ReflectionCore.Nodes;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace ReflectionCore
{
    public static class Worker
    {
        public static void Start() { }

        public static void Do(IBlock block)
        {
            block.BeforeDo();

            Do(block.StartNode);
        }

        public static void Do(IBlockNode blockNode, bool force = false)
        {
            while (blockNode != null)
            {
                if (force)
                    blockNode.Clear();

                DoMethod(blockNode, out blockNode);
            }
        }

        private static object DoMethod(IBlock block)
        {
            block.BeforeDo();
            IBlockNode blockNode = block.StartNode;

            while (blockNode != null)
            {
                object value = DoMethod(blockNode, out blockNode);

                if (NodeHelper.IsReturn(blockNode.Node))
                    return value;
            }

            return null;
        }

        private static object DoMethod(IBlockNode blockNode, out IBlockNode next)
        {
            next = null;
            object value = null;

            try
            {
                next = blockNode.NextNode;

                if (blockNode.Node.Type == NodeType.StaticParam || blockNode.WasInvoke)
                    return blockNode.Value;

                if (blockNode.Node is ICustomNode customNode)
                {
                    foreach (IBlockNode param in customNode.Block.In)
                        param.Invoke(DoMethod(param, out IBlockNode o));

                    value = DoMethod(customNode.Block);
                }
                else
                    value = NodeHelper.Invoke(
                        blockNode.Node.Name,
                        blockNode.In?.Select(i => DoMethod(i.BlockNode, out IBlockNode o)).ToArray()
                    );

                blockNode.Invoke(value);

                if (blockNode.Node.Out.Type == PinType.Thread && value is NodeThread thread)
                {
                    if (blockNode.NextNodes.Count() > 2)
                        Logger.Log($"{blockNode.NextNodes.Count()} thread in {blockNode.Node.Name}", new ArgumentException("Many thread"));

                    next = blockNode.NextNodes.ElementAt(1);

                    if (thread.IsAsync)
                    {
                        if (next.Node.Out.IsVoid)
                        {
                            IBlockNode block = next;
                            Task.Run(() => DoMethod(block, out IBlockNode o));

                            next = next.NextNode;
                            return null;
                        }
                        else
                            Logger.Log($"No void async method {blockNode.Node.Name}");
                    }
                }

                return value;
            }
            catch (Exception ex)
            {
                Logger.Log($"Do method {blockNode.Node.Name}", ex);
            }

            return value;
        }
    }
}
