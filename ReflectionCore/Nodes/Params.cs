﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        [Method(NodeType.StaticParam, NodeColor.LightRed, false, false)]
        public bool Bool()
            => default;

        [Method(NodeType.StaticParam, NodeColor.LightBlue, false, false)]
        public string String()
            => default;

        [Method(NodeType.StaticParam, NodeColor.Blue, false, false)]
        public double Number()
            => default;
    }
}
