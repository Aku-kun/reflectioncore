﻿using ReflectionCore.Model;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
       [Method]
       [OutName("True")]
       [ReturnName("False")]
        public NodeThread If(bool condition)
           => condition == true ? null : new NodeThread();
    }
}
