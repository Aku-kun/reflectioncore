﻿using Log;

using ReflectionCore.Interface;
using ReflectionCore.Model;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        public static IEnumerable<INode> AllNodes { get; }
        public static IEnumerable<INode> VisualNodes { get; }

        public static object Invoke(string name, object[] @params)
        {
            MethodInfo method = typeof(NodeHelper).GetMethod(name);

            if (method != null)
                return method?.Invoke(Instane, @params);
            else if (VisualNodes.Any(n => n.Name == name) && @params.Length > 0)
            {
                object obj = @params.First();
                method = obj.GetType().GetMethod(name);

                if (method != null)
                    return method?.Invoke(obj, @params.Skip(1).ToArray());
            }

            return null;
        }

        static NodeHelper()
        {
            AllNodes = GetAll();
            VisualNodes = GetVisualAll();

            Instane = new NodeHelper();
        }
        private static NodeHelper Instane { get; }

        public static PinType GetPinType(Type type)
        {
            if (type == typeof(NodeThread) || type == typeof(IBlockNode))
                return PinType.Thread;

            if (type == typeof(void))
                return PinType.Void;

            if (type == typeof(bool))
                return PinType.Bool;

            if (type == typeof(int) || type == typeof(float) || type == typeof(double))
                return PinType.Number;

            if (type == typeof(string))
                return PinType.String;

            if (type == typeof(IObject) || type.GetInterface(nameof(IObject)) != null)
                return PinType.IObject;

            if (type.GetInterface(nameof(IEnumerable)) != null)
                return PinType.Collection;

            return PinType.Object;
        }

        private static IOutPin GetOutPin(MethodAttribute method, ParameterInfo parameter, string name)
            => method.Type == NodeType.StaticParam || method.Type == NodeType.InputParam
                ? new StaticPin(GetPinType(parameter.ParameterType), GetDefaultValue(parameter.ParameterType))
                : new OutPin(GetPinType(parameter.ParameterType), name ?? "Out");

        private static object GetDefaultValue(Type t)
            => t.IsValueType
            ? Activator.CreateInstance(t)
            : null;

        private static IPin GetPin(ParameterInfo parameter)
            => new InPin(GetPinType(parameter.ParameterType), parameter.Name);

        private static IThreadPin GetThreadIn(MethodAttribute method)
            => new ThreadPin(isVisible: method.IsThreadIn);

        private static IThreadPin GetThreadOut(MethodAttribute method, string name = null)
            => new ThreadPin(name, method.IsThreadOut);

        private static Node GetNode(MethodInfo methodInfo)
        {
            MethodAttribute method = methodInfo.GetCustomAttribute<MethodAttribute>();

            if (method == null)
                return null;

            ReturnNameAttribute returnName = methodInfo.GetCustomAttribute<ReturnNameAttribute>() ?? new ReturnNameAttribute(null);
            OutNameAttribute outName = methodInfo.GetCustomAttribute<OutNameAttribute>() ?? new OutNameAttribute(null);

            return new Node(
                methodInfo.Name,
                method.Type,
                method.Color,
                GetThreadIn(method),
                GetThreadOut(method, outName.Name),
                GetOutPin(method, methodInfo.ReturnParameter, returnName.Name),
                methodInfo.GetParameters().Select(p => GetPin(p)).ToList()
            );
        }

        public static Node GetNode(string name)
        {
            try
            {
                return GetNode(typeof(NodeHelper).GetMethod(name));
            }
            catch (Exception ex)
            {
                Logger.Log($"GetNode {name}", ex);
            }

            return null;
        }


        private static List<INode> GetAll()
        {
            try
            {
                List<INode> result = new List<INode>();

                foreach (MethodInfo method in typeof(NodeHelper).GetMethods())
                {
                    MethodAttribute m = method.GetCustomAttribute<MethodAttribute>();

                    if (m == null)
                        continue;

                    if (m.Type == NodeType.Start || m.Type == NodeType.End || method.Name == nameof(NodeHelper.Method) || method.Name == nameof(NodeHelper.Return))
                        continue;

                    result.Add(GetNode(method));
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Log($"GetAll", ex);
            }

            return null;
        }

        public static List<INode> GetProgramStart()
        {
            try
            {
                return new List<INode>
                {
                    GetNode(nameof(NodeHelper.Start)),
                    GetNode(nameof(NodeHelper.End))
                };
            }
            catch (Exception ex)
            {
                Logger.Log($"GetProgramStart", ex);
            }

            return null;
        }

        public static List<INode> GetCustomMethodStart()
        {
            try
            {
                return new List<INode>
                {
                    GetNode(nameof(NodeHelper.Method)),
                    GetNode(nameof(NodeHelper.Return)),
                };
            }
            catch (Exception ex)
            {
                Logger.Log($"GetCustomMethodStart", ex);
            }

            return null;
        }

        [Method]
        public void Log(object data)
            => Logger.Log(data);
    }
}
