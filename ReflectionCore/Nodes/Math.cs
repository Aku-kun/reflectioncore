﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        [Method(NodeType.Method)]
        public double Sum(double x, double y)
            => x + y;
    }
}
