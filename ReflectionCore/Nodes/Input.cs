﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        public static bool IsMethod(INode node)
            => node.Name == nameof(Method);

        public static bool IsReturn(INode node)
            => node.Name == nameof(Return);

        [Method(NodeType.Start, NodeColor.Green, false)]
        public void Method() { }

        [Method(NodeType.End, NodeColor.Green, isThreadOut: false)]
        public void Return(object value) { }

        [Method(NodeType.InputParam, NodeColor.Default, false, false)]
        public bool InputBool()
            => default;

        [Method(NodeType.InputParam, NodeColor.Default, false, false)]
        public int InputInt()
            => default;

        [Method(NodeType.InputParam, NodeColor.Default, false, false)]
        public double InputDouble()
            => default;

        [Method(NodeType.InputParam, NodeColor.Default, false, false)]
        public double InputString()
            => default;

        [Method(NodeType.InputParam, NodeColor.Default, false, false)]
        public object InputObject()
            => default;
    }
}
