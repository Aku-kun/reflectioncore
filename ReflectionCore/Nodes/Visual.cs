﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        private static List<INode> GetVisualAll()
        {
            List<INode> result = new List<INode>();

            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                foreach (Type type in a.GetTypes())
                    if (type.IsInterface && type.GetCustomAttribute<VisualMethodsAttribute>() != null)
                        foreach (MethodInfo method in type.GetMethods())
                            result.Add(GetVisualNode(method));

            return result;
        }

        private static Node GetVisualNode(MethodInfo methodInfo)
            => new Node(
                methodInfo.Name,
                NodeType.ControlFunc,
                NodeColor.Orange,
                new ThreadPin(isVisible: true),
                new ThreadPin(isVisible: true),
                new OutPin(GetPinType(methodInfo.ReturnType), "Out"),
                new List<IPin> { new InPin(PinType.IObject, "Control") }
                    .Concat(methodInfo.GetParameters().Select(p => GetPin(p)))
                    .ToList()
            );
    }
}
