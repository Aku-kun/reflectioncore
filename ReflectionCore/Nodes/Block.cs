﻿using ReflectionCore.Interface;
using ReflectionCore.Model;

namespace ReflectionCore.Nodes
{
    public partial class NodeHelper
    {
        [Method(NodeType.Start, NodeColor.Red, false)]
        public void Start() { }

        [Method(NodeType.End, NodeColor.Red, isThreadOut: false)]
        public void End() { }
    }
}
