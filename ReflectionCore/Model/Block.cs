﻿using ReflectionCore.Interface;
using ReflectionCore.Nodes;

using RSerializer.Common;

using System.Collections.Generic;
using System.Linq;

namespace ReflectionCore.Model
{
    [RSerializable]
    public class Block : IBlock, IReference
    {
        public int Id { get; set; }

        public Block()
            => Items = new List<IBlockNode>();

        [Ignore]
        public IBlockNode StartNode { get; set; }
        [Ignore]
        public IBlockNode EndNode { get; set; }
        [Ignore]
        public IEnumerable<IBlockNode> In { get; set; }

        public IList<IBlockNode> Items { get; set; }

        private bool isInit;
        public void Init()
        {
            if (isInit)
                return;

            isInit = true;

            StartNode = Items.FirstOrDefault(n => n.Node.Type == NodeType.Start);
            EndNode = Items.FirstOrDefault(n => n.Node.Type == NodeType.End || NodeHelper.IsReturn(n.Node));

            In = Items.Where(n => n.Node.Type == NodeType.InputParam);
        }

        public void BeforeDo()
        {
            Init();

            foreach (IBlockNode blockNode in Items)
            {
                if (blockNode.Node is ICustomNode customNode)
                    customNode.Block.BeforeDo();
                else
                    blockNode.Clear();
            }
        }

        [Ignore]
        public IBlockNode this[int key]
        {
            get => Items[key];
            set => Items[key] = value;
        }
    }

    [RSerializable]
    public class BlockNode : IBlockNode, IReference
    {
        public int Id { get; set; }

        public BlockNode() { }

        public INode Node { get; set; }

        [Ignore]
        public IBlockNode NextNode => NextNodes?.FirstOrDefault();
        [Reference]
        public IEnumerable<IBlockNode> NextNodes { get; set; }
        [Reference]
        public IEnumerable<IBlockNodePin> In { get; set; }

        [Ignore]
        public bool WasInvoke { get; set; }
        public void Invoke(object value)
        {
            WasInvoke = true;
            Value = value;
        }
        public void Clear()
        {
            WasInvoke = false;
            if (Node.Type != NodeType.StaticParam)
                Value = null;
        }

        public object Value { get; set; }
    }

    [RSerializable]
    public class BlockNodePin : IBlockNodePin
    {
        public IPin FromPin { get; set; }
        public IPin ToPin { get; set; }

        [Reference]
        public IBlockNode BlockNode { get; set; }
    }
}
