﻿using ReflectionCore.Interface;
using ReflectionCore.Nodes;

using RSerializer.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionCore.Model
{
    [RSerializable]
    public class PObject : IObject, IReference
    {
        public int Id { get; set; }

        [Ignore]
        public IList<IPin> Properties { get; }
        [Ignore]
        public IList<IOutPin> Pins { get; }

        public PObject()
        {
            Properties = new List<IPin>();
            Pins = new List<IOutPin>();

            foreach (PropertyInfo property in GetType().GetProperties().OrderBy(p => p.PropertyType.ToString()).ThenBy(p => p.Name))
            {
                if (property.SetMethod == null)
                    continue;

                if (property.GetCustomAttribute<ObjectPinAttribute>() != null)
                    Pins.Add(new OutPin(NodeHelper.GetPinType(property.PropertyType), property.Name));

                if (property.GetCustomAttribute<ObjectPropertyAttribute>() != null)
                    Properties.Add(new InPin(NodeHelper.GetPinType(property.PropertyType), property.Name));
            }
        }

        public virtual object GetValue(string name)
        {
            PropertyInfo property = GetType().GetProperty(name);

            return property?.GetValue(this) 
                ?? (property.PropertyType.IsValueType 
                    ? Activator.CreateInstance(property.PropertyType)
                    : null);
        }
        public virtual void SetValue(string name, object value)
        {
            PropertyInfo property = GetType().GetProperty(name);

            if (value is IConvertible)
                property?.SetValue(this, Convert.ChangeType(value, property.PropertyType));
            else
                property?.SetValue(this, value);
        }
    }
}
