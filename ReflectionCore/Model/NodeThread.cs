﻿using RSerializer.Common;

namespace ReflectionCore.Model
{
    //TODO: Для циклов у выхода определенного типа выполняются все действия после продолжение с цикла (действия break continue)
    [RSerializable]
    public class NodeThread
    {
        public NodeThread(bool isAsync = false)
            => IsAsync = isAsync;

        public bool IsAsync { get; set; }
    }
}
