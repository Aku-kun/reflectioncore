﻿using ReflectionCore.Interface;

using RSerializer.Common;

using System.Collections.Generic;
using System.Linq;

namespace ReflectionCore.Model
{
    [RSerializable]
    public class Node : INode
    {
        public Node() { }

        public Node(string name, NodeType type, NodeColor color, IThreadPin threadIn, IThreadPin threadOut, IOutPin @out, IList<IPin> @in)
        {
            Type = type;
            Color = color;

            Name = name;

            ThreadIn = threadIn;
            ThreadOut = threadOut;
            Out = @out;
            In = @in;
        }

        public NodeType Type { get; set; }
        public NodeColor Color { get; set; }

        public string Name { get; set; }

        public IThreadPin ThreadIn { get; set; }
        public IThreadPin ThreadOut { get; set; }

        public IEnumerable<IPin> In { get; set; }
        public IOutPin Out { get; set; }
    }

    [RSerializable]
    public class CustomNode : Node, ICustomNode
    {
        public CustomNode() { }

        public CustomNode(string name, IBlock block) : base(name, NodeType.CustomFunc, NodeColor.Green, null, null, null, null)
        {
            Block = block;

            ThreadIn = new ThreadPin(null, true);
            ThreadOut = new ThreadPin(null, true);
            In = block.In.Select(n => (IPin)new InPin(n.Node.Out.Type, n.Value?.ToString())).ToList();
            Out = new OutPin(PinType.Object, block.EndNode.Node.Name, !block.EndNode.In.Any());
        }

        [Reference]
        public IBlock Block { get; set; }
    }
}
