﻿using ReflectionCore.Interface;

using RSerializer.Common;

namespace ReflectionCore.Model
{
    [RSerializable]
    public class InPin : IPin
    {
        public InPin() { }

        public InPin(PinType type, string name)
        {
            Type = type;
            Name = name;
        }

        public PinType Type { get; set; }
        public string Name { get; set; }
    }

    [RSerializable]
    public class OutPin : InPin, IOutPin
    {
        public OutPin() { }

        public OutPin(PinType type, string name = null, bool isVoid = false) : base(type, name)
            => IsVoid = isVoid;

        public OutPin(PinType type, string name = null) : this(type, name, type == PinType.Void) { }

        public bool IsVoid { get; set; }
    }

    [RSerializable]
    public class ThreadPin : InPin, IThreadPin
    {
        public ThreadPin() { }

        public ThreadPin(string name = null, bool isVisible = false) : base(PinType.Thread, name) 
            => IsVisible = isVisible;

        public bool IsVisible { get; set; }
    }

    [RSerializable]
    public class StaticPin : OutPin, IStaticPin
    {
        public StaticPin() { }

        public StaticPin(PinType type, object value) : base(type, null)
            => Value = value;

        public object Value { get; set; }
    }
}
