﻿using ReflectionCore.Interface;

using System;

namespace ReflectionCore.Model
{
    public class MethodAttribute : Attribute
    {
        public NodeType Type { get; }
        public NodeColor Color { get; }

        public bool IsThreadIn { get; }
        public bool IsThreadOut { get; }

        public MethodAttribute(NodeType type = NodeType.Method, NodeColor color = NodeColor.Default, bool isThreadIn = true, bool isThreadOut = true)
        {
            Type = type;
            Color = color;

            IsThreadIn = isThreadIn;
            IsThreadOut = isThreadOut;
        }
    }

    public class ReturnNameAttribute : Attribute
    {
        public string Name { get; }

        public ReturnNameAttribute(string name)
            => Name = name;
    }

    public class OutNameAttribute : Attribute
    {
        public string Name { get; }

        public OutNameAttribute(string name)
            => Name = name;
    }

    public class ObjectPinAttribute : Attribute { }
    public class ObjectPropertyAttribute : Attribute
    {
        public int Index { get; }

        public ObjectPropertyAttribute() { }
        public ObjectPropertyAttribute(int index)
            => Index = index;
    }
}
