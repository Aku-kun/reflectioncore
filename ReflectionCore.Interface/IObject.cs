﻿using System.Collections.Generic;

namespace ReflectionCore.Interface
{
    public interface IObject
    {
        int Id { get; set; }

        IList<IPin> Properties { get; }
        IList<IOutPin> Pins { get; }

        object GetValue(string name);
        void SetValue(string name, object value);
    }
}
