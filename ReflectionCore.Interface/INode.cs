﻿using System.Collections.Generic;

namespace ReflectionCore.Interface
{
    public interface INode
    {
        NodeType Type { get; }
        NodeColor Color { get; }

        string Name { get; }

        IThreadPin ThreadIn { get; }
        IThreadPin ThreadOut { get; }

        IEnumerable<IPin> In { get; }
        IOutPin Out { get; }
    }

    public interface ICustomNode : INode
    {
        IBlock Block { get; }
    }
}
