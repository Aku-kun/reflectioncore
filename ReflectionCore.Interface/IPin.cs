﻿namespace ReflectionCore.Interface
{
    public interface IPin
    {
        PinType Type { get; }
        string Name { get; }
    }

    public interface IOutPin : IPin
    {
        bool IsVoid { get; }
    }

    public interface IThreadPin : IPin
    {
        bool IsVisible { get; }
    }

    public interface IStaticPin : IPin
    {
        object Value { get; set; }
    }
}
