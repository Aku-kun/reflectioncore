﻿using System;

namespace ReflectionCore.Interface
{
    public class VisualMethodsAttribute : Attribute { }

    [VisualMethods]
    public interface IPageMethod
    {
        void ShowAlert(string message);
        void ShowWait();
        void HideWait();
    }

    [VisualMethods]
    public interface IButtonMethod
    {
        void Show();
        void Hide();

        void Enable();
        void Disable();
    }
}
