﻿namespace ReflectionCore.Interface
{
    public enum PinType
    {
        Thread = -1,
        Void = 0,

        Object = 1,
        Bool = 2,
        Number = 3,
        String = 4,

        IObject = 5,

        Collection = 6
    }

    public enum NodeColor
    {
        Default = 0,
        Red = 1,
        LightRed = 2,
        Blue = 3,
        LightBlue = 4,
        Green = 5,
        LightGreen = 6,
        Orange = 7
    }

    public enum NodeType
    {
        ControlFunc = -3,
        Control = -2,
        CustomFunc = -1,
        Method = 0,
        StaticParam = 1,
        InputParam = 2,
        Start = 3,
        End = 4
    }
}
