﻿using System.Collections.Generic;

namespace ReflectionCore.Interface
{
    public interface IBlock
    {
        int Id { get; set; }

        IBlockNode StartNode { get; }
        IBlockNode EndNode { get; }
        IEnumerable<IBlockNode> In { get; }

        IList<IBlockNode> Items { get; }

        void Init();
        void BeforeDo();
    }

    public interface IBlockNode
    {
        INode Node { get; }

        IBlockNode NextNode { get; }
        IEnumerable<IBlockNode> NextNodes { get; }
        IEnumerable<IBlockNodePin> In { get; }

        bool WasInvoke { get; }
        void Invoke(object value);
        void Clear();

        object Value { get; }
    }

    public interface IBlockNodePin
    {
        IPin FromPin { get; }
        IPin ToPin { get; }
        IBlockNode BlockNode { get; }
    }
}
