﻿using Config;

using MahApps.Metro;

using MaterialDesignThemes.Wpf;

using ReflectionCore;
using ReflectionCore.Visual;

using RSerializer.Common;

using System.Diagnostics;
using System.Windows;
using System.Windows.Media;

namespace Reflection.Client
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Worker.Start();

            string path = Process.GetCurrentProcess().MainModule.FileName;
            path = path.Remove(path.Length - SettingsConfig.EXE.Length, SettingsConfig.EXE.Length) + SettingsConfig.CONFIG;

            MainController.Config = ProjectConfig.Load(SerializerHelper.Load(path));
            LangSupport.LangConfig.Instane = MainController.Config.Lang;

            Color primaryColor = VisualHelper.FromArgb(MainController.Config.Visual.PrimaryColor).ToMediaColor();
            Color secondaryColor = VisualHelper.FromArgb(MainController.Config.Visual.SecondaryColor).ToMediaColor();

            IBaseTheme baseTheme = Theme.Light;
            string themeName = "BaseLight";

            if (MainController.Config.Visual.IsDark)
            {
                themeName = "BaseDark";
                baseTheme = Theme.Dark;
            }

            ITheme theme = Theme.Create(baseTheme, primaryColor, secondaryColor);
            new PaletteHelper().SetTheme(theme);

            ThemeManager.ChangeAppStyle(this,
                ThemeManager.GetAccent("Steel"),
                ThemeManager.GetAppTheme(themeName));

            base.OnStartup(e);
        }
    }
}
