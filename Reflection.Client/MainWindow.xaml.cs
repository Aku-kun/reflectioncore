﻿using Config;

using MahApps.Metro.Controls.Dialogs;

using ReflectionCore.Visual.Interface;
using ReflectionCore.Visual.Wpf;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Reflection.Client
{
    public partial class MainWindow : IPageManager
    {
        private MainController Controller => DataContext as MainController;

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainController(this);
            OnSelect(Controller.Pages.FirstOrDefault());
        }

        public Action<IPage> OnSelect => p =>
        {
            if (p == null || Controller.SelectPage == p)
                return;

            if (Controller.SelectPage != null)
                Controller.SelectPage.IsSelect = false;
            
            Controller.SelectPage = p;
            Controller.SelectPage.IsSelect = true;

            Controller.Notify(nameof(MainController.SelectPage));
        };

        public async void ShowAlert(string message)
        {
            DialogCoordinator.Instance.ShowModalMessageExternal(this, message, null);
        }

        public void ShowWait() => this.ShowWait();
        public void HideWait() => this.HideWait();
    }

    public class MainController : Base
    {
        public static ProjectConfig Config { get; set; }

        public List<IPage> Pages { get; }
        public IPage SelectPage { get; set; }

        public MainController(IPageManager pageManager)
        {
            Pages = Config.Pages.Select(p => WpfVisualHelper.GetView(p).Init(pageManager)).ToList();
            Notify(nameof(Pages));
        }
    }

    public class Base : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify([CallerMemberName]string name = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
