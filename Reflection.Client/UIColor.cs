﻿using System.Windows.Media;

using DColor = System.Drawing.Color;
using MColor = System.Windows.Media.Color;

namespace Reflection.Client
{
    /// <summary>
    /// *Set contains all material colors 
    /// https://www.materialpalette.com/colors
    /// </summary>
    public static class UIColor
    {
        public static SolidColorBrush ToBrush(this DColor color)
            => new SolidColorBrush(color.ToMediaColor());

        public static MColor ToMediaColor(this DColor color)
            => MColor.FromArgb(color.A, color.R, color.G, color.B);

        public static MColor ToMediaColor(this DColor color, byte a)
            => MColor.FromArgb(a, color.R, color.G, color.B);
    }
}
