﻿using ReflectionCore.Entity.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.SQL
{
    public interface ISQLManager : IDisposable
    {
        void SetConnection(DBConfig config);


    }
}
