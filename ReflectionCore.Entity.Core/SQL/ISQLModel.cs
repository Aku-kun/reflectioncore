﻿using ReflectionCore.Entity.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.SQL
{
    public interface ISQLModel
    {
        SQLObjects Type { get; set; }
        string Name { get; set; }

        ISQLModel Create(string objectName, params object[] parameters);

        ISQLModel Alter(string objectName, object parameter);

        ISQLModel Drop(string objectName);

        ISQLModel Count();

        ISQLModel Where(string field, object value);
        ISQLModel Where(string field, object value, string condition);
        ISQLModel Where(string field, object value, string condition, CondOperators oper);

        ISQLModel GroupBy(string field);

        ISQLModel OrderBy(string filed);
        ISQLModel OrderBy(string filed, SortOperators oper);

        ISQLModel Select(params string[] columns);

        ISQLModel Join(JoinTypes type, string rightTable, string leftField, string condition, string rightField);
        ISQLModel Join(string leftTable, JoinTypes type, string rightTable, string leftField, string condition, string rightField);

        ISQLModel Uodate(string[] fileds, object[] vaules);

        ISQLModel Insert(string[] fileds, object[] vaules);

        ISQLModel Delete();

        bool Has();
        bool Has(string table);

        void BuildQuery();

        object Execute();
    }
}
