﻿using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Data
{
    public interface IDataProvider
    {
        void Init(DBConfig config, ISQLManager sqlManager);
        object Execute(IAction[] actions);
    }
}
