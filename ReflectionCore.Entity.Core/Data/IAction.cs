﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Data
{
    public interface IAction
    {
        IModel[] Models { get; set; }
        Actions Action { get; set; }
    }
}
