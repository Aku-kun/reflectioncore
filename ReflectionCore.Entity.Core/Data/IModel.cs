﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Data
{
    public interface IModel
    {
        ICollection<Field> Fields { get; set; }
        string Name { get; set; }

        object GetValue(string name);
        string SetValue(string name, object value);
    }
}
