﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Data
{
    public class DBConfig
    {
        private string _dbName;
        private string _serverName;
        private string _userId;
        private string _password;

        public DBConfig(string dbName, string serverName, string userId, string password)
        {
            _dbName = dbName;
            _serverName = serverName;
            _userId = userId;
            _password = password;
        }

        public override string ToString()
        {
            return $"Server={_serverName};Database={_dbName};User ID={_userId};Password={_password}";
        }
    }
}
