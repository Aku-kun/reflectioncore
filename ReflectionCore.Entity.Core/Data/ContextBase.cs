﻿using ReflectionCore.Entity.Core.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Data
{
    public abstract class ContextBase
    {
        private DBConfig _config;

        protected ISQLManager SQLManager { get; private set; }

        public ContextBase(DBConfig config, ISQLManager sqlManager)
        {
            _config = config;
            SQLManager = sqlManager;

            SQLManager.SetConnection(config);
        }

        public abstract object Get(IModel[] models);
        public abstract object Set(IModel[] models);
        public abstract object Delete(IModel[] models);
        public abstract object Update(IModel[] models);
        public abstract object Create(IModel[] models);
        public abstract object Alter(IModel[] models);
        public abstract object Drop(IModel[] models);
    }
}