﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Migrator
{
    public interface IMigrationManager
    {
        void Load();
        void Save();
    }
}
