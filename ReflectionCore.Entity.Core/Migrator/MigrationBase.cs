﻿using ReflectionCore.Entity.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core.Migrator
{
    public abstract class MigrationBase
    {
        protected IModel _entity;

        public string Name { get; private set; }
        public DateTimeOffset MigrationDate { get; private set; }
        public Operations Operation { get; set; }

        public MigrationBase(IModel entity)
        {
            _entity = entity;

            MigrationDate = DateTimeOffset.Now;
            Name = 
                MigrationDate.ToString() 
                + "_" + Operation.ToString() 
                + "_" + _entity.Name;
        }

        public abstract string Apply();
    }
}
