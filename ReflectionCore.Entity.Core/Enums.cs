﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionCore.Entity.Core
{
    public enum Actions
    {
        Get,
        Set,
        Delete,
        Update,

        Create,
        Alter,
        Drop
    }

    public enum Operations
    {
        Add,
        Edit,
        Remove,
    }

    public enum CondOperators
    {
        AND,
        OR,
        NOT
    }

    public enum SortOperators
    {
        DESC,
        ASC
    }

    public enum JoinTypes
    {
        INNER,
        LEFT,
        RIGHT,
        FULL
    }

    public enum SQLObjects
    {
        DATABASE,
        TABLE,
        VIEW,
        INDEX
    }
}
