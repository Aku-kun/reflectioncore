﻿using Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using RSerializer.Common;
using System;
using System.Collections.Generic;

namespace RSerializer.Tests
{
    [RSerializable]
    public class Test
    {
        public string Value { get; set; }
        public List<string> TestList { get; set; }
        public Dictionary<string, string> TestDictinary { get; set; }
        public Dictionary<int, string> TestIntDictinary { get; set; }

        public void Init()
        {
            Value = "TestString 123 {<>}";
            TestList = new List<string>
            {
                "TestString 123 {<>} (1)",
                "TestString 123 {<>} (2)"
            };
            TestDictinary = new Dictionary<string, string>
            {
                { "TestString 123 {<>} (1)", "TestString 123 {<>} (1)" },
                { "TestString 123 {<>} (2)", "TestString 123 {<>} (2)"}
            };
            TestIntDictinary = new Dictionary<int, string>
            {
                { 1, "TestString 123 {<>} (1)" },
                { 2, "TestString 123 {<>} (2)"}
            };
        }
    }

    [RSerializable]
    public class Test2
    {
        public string Value { get; set; } = "jj";
        public decimal Value2 { get; set; } = 9;
    }

    [TestClass()]
    public class SerializerTests
    {
        [TestMethod]
        public void DateTimeTest()
        {
            string data;
            SettingsConfig settings = new SettingsConfig
            {
                LastUpdate = DateTime.Now
            };

            using (Serializer serializer = new Serializer())
            {
                data = serializer.Serialize(new Test2());
            }

            using (Serializer serializer = new Serializer())
            {
                SettingsConfig set = serializer.Deserialize<SettingsConfig>(data);
            }
        }

        [TestMethod()]
        public void DeserializeTest()
        {
            string data = SerializerHelper.Load();
            Test test = null;

            using (Serializer serializer = new Serializer())
            {
                test = serializer.Deserialize<Test>(data);
            }

            Assert.IsTrue(test != null && !string.IsNullOrEmpty(test.Value) && test.TestDictinary.Count > 0);
        }

        [TestMethod()]
        public void SerializeTest()
        {
            string data = null;
            Test test = new Test();
            test.Init();

            using (Serializer serializer = new Serializer())
            {
                data = serializer.Serialize(test);

                SerializerHelper.Save(data);
            }

            Assert.IsTrue(!string.IsNullOrEmpty(data));
        }
    }
}