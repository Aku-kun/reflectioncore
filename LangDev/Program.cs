﻿using Config;

using LangSupport;

using ReflectionCore.Visual;

using RSerializer.Common;

namespace Dev
{
    class Program
    {
        static VisualConfig visual;
        static LangConfig langConfig;

        static void Main(string[] args)
        {
            visual = new VisualConfig
            {
                IsDark = true,
                PrimaryColor = VisualHelper.TealSet.Color500.ToArgb(),
                SecondaryColor = VisualHelper.TealSet.Color500.ToArgb(),
            }; 
            
            langConfig = new LangConfig();

            Lang ru = langConfig.AddLang("ru");
            Lang en = langConfig.AddLang("en");

            langConfig.CurrentLang = en;

            Add("general", "Общие", "General");
                Add("settings", "Настройки", "Settings");
                Add("langs", "Языки", "Langs");
                Add("save", "Сохранить", "Save");
                Add("load", "Загрузить", "Load");
            Add("pages", "Страницы", "Pages");
                Add("design", "Дизайн", "Design");
                Add("code", "Код", "Code");
                Add("main", "Основные", "Main");
                Add("other", "Другие", "Other");
            Add("funcs", "Функции", "Functions");

            Add("name", "Название", "Name");
            Add("value", "Значение", "Value");

            Add("add", "Добавить", "Add");
            Add("remove", "Удалить", "Remove");

            Add("add_node", "Добавить нод", "Add node");
            Add("add_control", "Добавить контрол", "Add control");

            Add("compile", "Собрать", "Compile");
            Add("refresh", "Обновить", "Refresh");
            Add("history", "История", "History");

            Add("project_load_error", "Папка не является проектом или не содержит необходимых файлов", "The folder is not a project or does not contain the necessary files");
            Add("project_save_error", "Ошибка при сохранении проекта", "Error saving project");
            Add("name_error", "Имя уже существует", "Name already exists");
            Add("rec_error", "Нельзя использовать нод внутри себя", "You cannot use a node inside yourself");
            Add("input_node_error", "Нельзя использовать нод ввода", "You cannot use an input node");
            Add("project_build_error", "Ошибка компиляции", "Compilation error");

            Add("build", "Сборка проекта", "Build project");
            Add("build_start", "Сборка", "Build");
            Add("build_end", "Сборка завершина", "Build success");
            Add("project_save", "Проект сохранен", "Project save");
            Add("project_load", "Проект загружен", "Project load");
            Add("build_complite", "Сборка проекта завершена", "Build project complite");

            Add("new_prolect", "Создать новый проект", "Create a new project");
            Add("open_prject", "Открыть существующий проект", "Open an existing project");

            Add("today", "Сегодня", "Today");
            Add("search", "Поиск", "Search");

            ProjectConfig config = new ProjectConfig
            {
                Visual = visual,
                Lang = langConfig
            };

            SerializerHelper.Save(config.Save(), @"D:\Users\Aku\Documents\Work\Lib\ReflectionCore\Reflection.IDE\bin\Debug\.rc");
        }

        static void Add(string key, string ru, string en)
        {
            langConfig.AddWord(key);
            langConfig.SetValue(key, "ru", ru);
            langConfig.SetValue(key, "en", en);
        }
    }
}
